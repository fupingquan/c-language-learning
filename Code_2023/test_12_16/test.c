#define _CRT_SECURE_NO_WARNINGS 1
//author:付平权
//description:
//time:

//int main()
//{
//	3 + 5;
//	printf("hehe");
//	; //空语句
//	return 0;
//}

#include <stdio.h>

//int main()
//{
//	int age = 0;
//	//输入
//	scanf("%d", &age);
//
//	if (age < 18)
//	{
//		printf("未成年\n");
//	}
//	else
//		printf("你已经成年啦\n");
//
//	return 0;
//}


////判断年龄情况
//int main()
//{
//	int age = 0;
//	//输入
//	printf("请输入您的年龄：");
//	scanf("%d", &age);
//
//	if (age < 18)
//		printf("请少年\n");
//	else if (age >= 18 && age <= 30)
//		printf("青年\n");
//	else if (age >= 31 && age <= 50)
//		printf("中年\n");
//	else if (age >= 51 && age <= 80)
//		printf("中老年\n");
//	else if (age >= 81 && age <= 100)
//		printf("老年\n");
//	else
//		printf("老寿星\n");
//	return 0;
//}

////另一种写法
//int main()
//{
//	int age = 0;
//	printf("请输入年龄：");
//	scanf("%d", &age);
//
//	if (age < 18)
//		printf("青少年\n");
//	else
//		if (age >= 18 && age <= 30)
//			printf("青年\n");
//		else if (age >= 31 && age <= 50)
//			printf("中年\n");
//		else if (age >= 51 && age <= 80)
//			printf("中老年\n");
//		else if (age >= 81 && age <= 100)
//			printf("老年\n");
//		else
//			printf("老寿星\n");
//
//	return 0;
//
//}

int main()
{
	char* c[] = { "ENTER","NEW","POINT","FIRST" };
	char** cp[] = { c + 3,c + 2,c + 1,c };
	char*** cpp = cp;

	printf("%s\n", **++cpp);  //POINT
	printf("%s\n", *-- * ++cpp + 3);  //ER
	printf("%s\n", *cpp[-2] + 3);  //ST
	printf("%s\n", cpp[-1][-1] + 1);  //EW

	return 0;
}
