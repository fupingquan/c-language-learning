#define _CRT_SECURE_NO_WARNINGS 1
//author:付平权
//description:
//time:2023年12月10日

#include <stdio.h>
#include  <string.h>

//int main()
//{
//
//	printf("%zd\n", strlen("c:\test\121)"));
//	return 0;
//
//}

//int main()
//{
//	int seconds = 0;
//	int h = 0;
//	int m = 0;
//	int s = 0;
//	//输入
//	scanf("%d",&seconds);
//	//计算
//	h = seconds / 3600;
//	m = (seconds % 3600) / 60;
//	s = seconds % 60;
//	//输出
//	printf("%d %d %d\n", h, m, s);
//
//	return 0;
//}


//int main()
//{
//	int a = 100;
//	//sizeof是一个操作符，不是函数
//	//sizeof计算的是变量占有内存的大小，单位是字节
//	printf("%d\n",sizeof(a));
//	printf("%d\n", sizeof a);  //变量时候可以不用括号
//
//	printf("%d\n", sizeof(int));
//	//printf("%d\n", sizeof int);  //err 这是一个错误示范
//
//	return 0;
//}


//int add(int x, int y) //形参
//{
//	int z = x + y;
//	return z;
//}
//
//int main()
//{
//	int a = 0;
//	int b = 0;
//
//	//输入
//	scanf("%d %d", &a, &b);
//
//	int sum = add(a, b);  //实参
//	//输出
//	printf("%d\n",sum);
//}


//int main()
//{
//	int a = 1;
//	int b = ++a;  //前置++，先++，后使用
//	//上行代码可以作如下分解，便于理解
//	//a = a + 1;
//	//b = a;
//
//	int c = 1;
//	int d = c++;  //后置++，先使用，后++
//	//c = d;
//	//c = c + 1;
//
//	printf("a=%d b=%d c=%d d=%d\n",a,b,c,d);
//	return 0;
//}