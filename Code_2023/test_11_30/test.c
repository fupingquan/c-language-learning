#define _CRT_SECURE_NO_WARNINGS 1
// function:2023年11月30日的作业代码
// author:付平权
// description:初学C语言的记录
// time:2023-11-30 12:45

//#include <stdio.h>
//int main()
//{
//    int arr[] = { 1,2,(3,4),5 };
//    printf("%d\n", sizeof(arr));  //16
//    return 0;
//}


//#include <stdio.h>
//int main()
//{
//	int a = 0, b = 0;
//	for (a = 1, b = 1; a <= 100; a++)
//	{
//		if (b >= 20) break;
//		if (b % 3 == 1)
//		{
//			b = b + 3;
//			continue;
//		}
//		b = b - 5;
//	}
//	printf("%d\n", a); //8
//	return 0;
//}



//#include <stdio.h>
//int main()
//{
//    char str[] = "hello bit";
//    printf("%d %d\n", sizeof(str), strlen(str));
//    return 0;
//}


//char acX[] = "abcdefg";
//char acY[] = { 'a','b','c','d','e','f','g' };



#include <stdio.h>
//int main() {
//    int i, j;  // i, j控制行或列 
//    for (i = 1; i <= 9; i++) {
//        for (j = 1; j <= 9; j++)
//            // %2d 控制宽度为两个字符，且右对齐；如果改为 %-2d 则为左对齐
//            // \t为tab缩进
//            printf("%d*%d=%2d\t", i, j, i * j);
//
//        printf("\n");
//    }
//
//    return 0;
//}


//修正后
int main()
{
	int i = 0;
	for (i = 1; i <= 9; i++)
	{
		int j = 0;
		for (j = 1; j <= i; j++)
		{
			printf("%d*%d=%2d ", j, i, j * i);
		}
		printf("\n");
	}
	return 0;
}