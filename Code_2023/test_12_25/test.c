#define _CRT_SECURE_NO_WARNINGS 1
//author:付平权
//description:
//time:

#include <stdio.h>

int fun(int n, int r)
{
	if (r > n)
		return 0;
	if (r == 0 || n == 0 || n == 1)
		return 1;
	else
		return fun(n - 1, r) + fun(n - 1, r - 1);
}

int main()
{
	int result = 0;
	int n = 8;
	int r = 2; 
	result = fun(n, r);
	printf("fun(%d,%d)=%d\n", n, r, result);
	return 0;
}

//解决gitee的乱码，修改newc++ 文件为UTF-8