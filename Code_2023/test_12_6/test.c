#define _CRT_SECURE_NO_WARNINGS 1
// function:初始指针
// author:付平权
// description:指针的相关学习
// time:2023-12-06 10:16

#include <stdio.h>

////指针就是地址
//int main()
//{
//	int a = 10; //a占四个字节
//
//	int* pa = &a; //拿到的是a的四个字节中第一个字节的地址
//
//	*pa = 20; //* 解引用操作  *pa就是通过pa里边的地址，找到a
//
//	printf("a=%d\n", a);
//
//	return 0;
//}


//int main()
//{
//	printf("%zd\n", sizeof(char*));  //64位环境下，8个字节
//	printf("%zd\n", sizeof(short*));
//	printf("%zd\n", sizeof(int*));
//	printf("%zd\n", sizeof(long*));
//	printf("%zd\n", sizeof(long long*));
//	printf("%zd\n", sizeof(float*));
//	printf("%zd\n", sizeof(double*));
//
//	return 0;
//}

//指针类型的意义
//1.指针类型决定了：指针解引用的权限有多大
//2.指针类型决定了,指针走一步，能走多远（步长）

//int main()
//{
//	int a = 5;
//	int* pa = &a;
//	*pa = 10; //改变四个字节
//
//	int c = 2;
//	char* pc = &c;
//	*pc = 8; //改变一个字节
//
//	return 0;
//}


//int main()
//{
//	int arr[10] = { 0 };
//	int* p = arr;
//	char* pc = arr;
//	printf("%p\n", p);
//	printf("%p\n", p+1);
//	printf("%p\n", pc);
//	printf("%p\n", pc+1);
//
//	return 0;
//
//}


//int main()
//{
//	int arr[10] = { 0 };
//	for (int i = 0; i < 10; i++)
//	{
//		scanf("%d", &arr[i]); //输入
//	}
//
//	int sum = 0;
//	for (int i = 0; i < 10; i++)
//	{
//		sum += arr[i];
//	}
//	printf("sum=%d\n", sum);
//
//	double avg = sum / 10.0;
//	printf("avg=%lf\n", avg);
//
//	return 0;
//}


//int main()
//{
//	int a;
//	while (scanf("%d", &a) != EOF){
//		for (int i = 0; i < a; i++)
//		{
//			for (int j = 0; j < a; j++)
//			{
//				if (i == j || i + j == a-1)
//				{
//					printf("*");
//				}
//
//				else
//					printf("  ");
//			}
//			printf("\n");
//		}
//	}
//
//	return 0;
//}

//struct S  //结构体
//{
//	char name[20];
//	int age;
//	float score;
//};
//
//void print1(struct S t)
//{
//	printf("%s %d %f\n", t.name, t.age, t.score);
//}
//
//void print2(struct S*ps)
//{
//	//printf("%s %d %f\n", (*ps).name, (*ps).age, (*ps).score);
//	printf("%s %d %f\n", ps->name, ps->age, ps->score);
//}
//
//int main()
//{
//	struct S s = { "zhangsan",20,85.5f };
//	print1(s);
//	print2(&s);
//	return 0;
//}


struct student 
{
	char name[20];
	int age;
	double weight;
};

void Print(struct student s)
{
	printf("%s %d %lf\n", s.name, s.age, s.weight);
}

void Print1(struct student *p)
{
	printf("%s %d %lf\n", p->name, p->age, p->weight);

}

int main()
{
	struct student stu1 = {"zhangsan",17,52.0} ;
	Print(stu1);
	Print1(&stu1);
	return 0;
}