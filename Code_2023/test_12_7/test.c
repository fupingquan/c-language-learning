#define _CRT_SECURE_NO_WARNINGS 1
// function:初阶
// author:付平权
// description:C语言的记录
// time:2023-12-07 17:16

#include <string.h>
//助记符--汇编语言--B语言--C语言
//面向过程

//库函数一定要包含头文件


////显示整数15和37的和
//#include <stdio.h>
////main函数
//int main(void)
//{
//	//程序的入口
//	printf("%d",15+37); //用十进制数显示15和37的和
//	
//	return 0;
//}

//#include <stdio.h>
//int main()
//{
//	printf("%d", 15 - 37); //-22
//	return 0;
//}

//#include <stdio.h>
//int main()
//{
//	printf("15与37的和是%d\n", 15 - 37); //-22
//	return 0;
//}


//#include <stdio.h>
//int main(void)
//{
//	printf("您好！\n我叫付平权。\n"); //中间和最后换行
//	//printf("您好！\n");
//	//printf("我叫付平权\n"); //字符串常量
//	return 0;
//}


//#include <stdio.h>
//int main()
//{
//	printf("天\n地\n人\n");
//	printf("嘿！\n您好！\n再见。\n");
//	return 0;
//}


//// 为两个变量赋整数值并显示
//#include <stdio.h>
//int main()
//{
//
//	int vx, vy;
//
//	vx = 57;
//	vy = vx + 10;
//
//	printf("vx的值是%d。\n",vx);
//	printf("vy的值是%d。\n",vy);
//
//	return 0;
//}

#include <stdio.h>

//int main()
//{
//	printf("%d",printf("%d",printf("%d",43)));  //4321
//	return 0;
//}

int main()
{
	printf("abc的字符长度=%zd\n", strlen("abc"));
	return 0;
}