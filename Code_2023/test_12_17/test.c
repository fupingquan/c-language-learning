#define _CRT_SECURE_NO_WARNINGS 1
//author:付平权
//description:
//time:

//#include <stdio.h>
//#include <string.h>
//
//int main()
//{
//	//int len = strlen("abcdef");
//	//printf("%d\n", len);
//
//	//链式访问
//	printf("%zd\n", strlen("abcdef"));
//
//	return 0;
//}

//#include <stdio.h>
//
//int main()
//{
//	printf("%d", printf("%d", printf("%d", 43)));  //4321
//	
//	printf("%d ", printf("%d ", printf("%d ", 43)));  //43 3 2
//
//	return 0;
//}


////递归示例1
//#include <stdio.h>
//
//void print(unsigned int n)
//{
//	if (n > 9)
//	{
//		print(n / 10);//调用自己
//	}
//	printf("%d ", n % 10);
//}
//
//int main()
//{
//	unsigned int num = 0;
//	//输入
//	scanf("%d", &num);
//
//	print(num); 
//
//	return 0;
//}


#include <stdio.h>
size_t my_strcpy(char* dest, const char* src)
{
	char* ret = dest;
	assert(dest != NULL);
	assert(src != NULL);

	while (*src != '\0')
	{
		*dest = *src;
		dest++;
		src++;
	}
	*dest=* src;// \0 得拷贝
	return ret;
}

////递归模拟实现strlen函数
//int my_strlen(char*s)
//{
//	if (*s == '\0')
//		return 0;
//	else
//		return 1 + my_strlen(s + 1);
//}


//int main()
//{
//	char arr[] ="abcd";
//
//	int len = my_strlen(arr);
//	printf("%d\n", len);
//	return 0;
//}


//#include <stdio.h>
////n的阶乘
//int Fac(int n)
//{
//	if (n < 1)
//		return 1;
//	else
//		return n * Fac(n - 1);
//}
//
//int main()
//{
//	int n = 0;
//	printf("请输入要计算的阶乘：");
//	scanf("%d", &n);
//	int ret = Fac(n);
//	printf("%d的阶乘是：%d\n", n,ret);
//
//	return 0;
//}


//#include <stdio.h>
////求第n个斐波那契数
////这个代码不能求太大数字的，会超过范围导致计算结果出错
//int Fib(int n)
//{
//	int a = 1;
//	int b = 1;
//	int c = 1;
//	while (n >= 3)
//	{
//		c = a + b;
//		a = b;
//		b = c;
//		n--;
//	}
//	return c;
//}
//
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int ret = Fib(n);
//	printf("%d\n", ret);
//
//	return 0;
//}


////这段代码VS中运行会报错，VS不支持变长数组
//#include <stdio.h>
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int arr[n];
//	int i = 0;
//	for (i = 0; i < n; i++)
//	{
//		arr[i] = i;
//	}
//	for (i = 0; i < n; i++)
//	{
//		printf("%d\n", arr[i]);
//	}
//	return 0;
//}