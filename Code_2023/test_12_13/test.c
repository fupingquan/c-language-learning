#define _CRT_SECURE_NO_WARNINGS 1
//author:付平权
//description:
//time:2023年12月26日

#include <stdio.h>

//int main()
//{
//	int i = 2;
//	switch (i)
//	{
//	case 1:i++;
//	case 2:i--;
//	case 3:++i; break;
//	case 4:--i;
//	default:i++;
//	}
//	printf("%d\n", i);
//	return 0;
//}

//条件表达式
int main()
{
	int a = 5, b = 4, c = 3, d;
	d = (a > b > c);  //0
	printf("%d\n", d);
}