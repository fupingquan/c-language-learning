#define _CRT_SECURE_NO_WARNINGS 1
// function:写成函数的二分法
// author:付平权
// description:自定义binary_search函数来实现二分
// time:2023-12-03 17:18

#include <stdio.h>
#include <string.h>
#include <math.h>

//写一个函数，实现一个整型有序数组的二分查找。
int binary_search(int arr[], int k,int sz)
{
	int left = 0;
	int right = sz-1;

	while (left <= right)
	{
		//int mid =(left+right)/2;
		int mid = left + (right - left) / 2;
		if (arr[mid] < k)
		{
			left = mid + 1;
		}
		else if (arr[mid] > k)
		{
			right = mid - 1;
		}
		else
		{
			return mid;
		}
	}
	return -1;
}

int main()
{
	int arr[] = { 0,1,2,3,4,5,6,7,8,9,10 };
	int k = 7; //要查找的数
	int sz = sizeof(arr) / sizeof(arr[0]);  //计算数组有几个元素
	//找到了：返回下标
	//找不到：返回-1
	int ret = binary_search(arr, k, sz);
	if (ret == -1)
		printf("找不到\n");
	else
		printf("找到了，下标是:%d\n", ret);
	return 0;

}
//注意点：1.要有序排列
		//2.计算数组的元素个数sizeof   '/'


////写一个函数，实现一个整形有序数组的二分查找。
//int binary_search(int arr[], int k, int sz)
//{
//	int left = 0;
//	int right = sz - 1;
//
//	while (left <= right)
//	{
//		//int mid = (left + right) / 2;
//		int mid = left + (right - left) / 2;
//		if (arr[mid] < k)
//		{
//			left = mid + 1;
//		}
//		else if (arr[mid] > k)
//		{
//			right = mid - 1;
//		}
//		else
//		{
//			return mid;
//		}
//	}
//	return -1;
//}

//int main()
//{
//	int arr[] = { 1,2,0,3,4,5,6,7,8,9,10 };
//	int k = 7;
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	//二分查找
//	//找到了：返回下标
//	//找不到：返回-1
//	int ret = binary_search(arr, k, sz);
//	if (ret == -1)
//		printf("找不到\n");
//	else
//		printf("找到了，下标是:%d\n", ret);
//
//	return 0;
//}

//int Add(int x, int y)//形式参数，简称形参
//{
//    return x + y;
//}
//
//int main()
//{
//    int a = 0;
//    int b = 0;
//    //输入
//    scanf("%d %d", &a, &b);
//    int r = Add(a, b);//a,b是真实传递给Add函数的参数
//    //a,b是实参（实际参数）
//    
//    //输出
//    printf("%d\n", r);
//    return 0;
//}

//void test(int n)
//{
//	if (n <= 0)
//		return;
//	int i = 0;
//	int sum = 0;
//	for (i = 1; i <= n; i++)
//	{
//		sum += i;
//	}
//	printf("%d\n", sum);
//}
//int main()
//{
//	test(5);
//	return 0;
//}

//int test()
//{
//	return 3.14;
//}
//
//int main()
//{
//	int r = test();
//	printf("%d\n", r);
//	return 0;
//}

//函数用来判断n是奇数还是偶数
//是奇数返回1
//是偶数返回0

//int test(int n)
//{
//	if (n % 2 == 1)
//		return 1;
//	else
//		return 0;
//}
//
//
//int main()
//{
//	int ret = test(6);
//
//	return 0;
//}


//假设我们计算某年某月有多少天？
//1  2  3  4  5  6  7  8  9  10 11 12
//31 28 31 30 31 30 31 31 30 31 30 31
//   29

//int is_leap_year(int y)
//{
//	if (((y % 4 == 0) && (y % 100 != 0)) || (y % 400 == 0))
//		return 1;//闰年返回1
//	else
//		return 0;//不是闰年返回0
//}

//#include <stdbool.h>
//
//_Bool is_leap_year(int y)
//{
//	if (((y % 4 == 0) && (y % 100 != 0)) || (y % 400 == 0))
//		return true;//闰年返回1
//	else
//		return false;//不是闰年返回0
//}
//
//int get_days_of_month(int y, int m)
//{
//	int days[] = { 0, 31,28,31,30,31,30,31,31,30,31,30,31 };
//	//             0  1  2  3  4  5  6  7  8
//	int day = days[m];
//	if (is_leap_year(y) && m == 2)
//		day += 1;
//
//	return day;
//}
//
//int main()
//{
//	int year = 0;
//	int month = 0;
//	scanf("%d %d", &year, &month);//2008 8
//	int days = get_days_of_month(year, month);
//	printf("%d\n", days);
//	return 0;
//}


//#include "add.h"
//
////导入静态库(了解就行)
//#pragma comment(lib, "add.lib") 
//
//int main()
//{
//	int a = 0;
//	int b = 0;
//	scanf("%d %d", &a, &b);
//	int ret = Add(a, b);
//	printf("%d\n", ret);
//
//	return 0;
//}