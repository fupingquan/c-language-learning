#define _CRT_SECURE_NO_WARNINGS 1
//author:付平权
//description:
//time:


//#include <stdio.h>
//
//int main()
//{
//	int arr[10] = { 0 };
//	printf("%zd\n", sizeof(arr));  //计算的是数组的总大小，单位是字节
//	printf("%zd\n", sizeof(arr[0]));
//
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	printf("%d",sz);
//
//	return 0;
//}


//#include <stdio.h>
//int main()
//{
//	int arr1[9] = { 1,2,3,4,5,6,7,8,9 };
//	int sz = sizeof(arr1) / sizeof(arr1[0]);
//
//	for (int i = 0; i < sz; i++)
//	{
//		printf("%p\n", &arr1[i]); //打印地址用%p
//	}
//	return 0;
//}


	//// 一维数组
	//int arr[10] = { 0 };
	//char arr1[] = { 'a','b','c' };

	////二维数组
	//int arr2[3][3] = { 1,2,3,2,3,4,3,4,5 };
	//int arr3[4][5] = { {1,2,3},{2,3,4},{3,4,5,6,7},{5,6,7,8,9} };

	////二维数组行是可以省略的，但是列是不能省略的
	//int arr4[][5] = { {1,2,3},{22,3,4},{3,4,5,6,7},{5,6,7,8,9} };


//#include <stdio.h>
//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6 };
//	printf("%p\n", arr);
//	printf("%p\n", arr + 1);
//
//	printf("%p\n", &arr[0]);
//	printf("%p\n", &arr[0] + 1);
//
//	printf("%p\n", &arr);  //数组的地址
//	printf("%p\n", &arr+1);  //+1，跳过整个数组
//
//	//printf("%p\n", sizeof(arr));
//	return 0;
//}


//汉诺塔问题
#include <stdio.h>

void print(char x, char y)
{
	printf(" %c->%c ", x, y);
}

void move(int n,char start,char temp,char end)
{
	if (n == 1)
		print(start, end);
	else
	{
		move(n - 1, start, end, temp);
		print(start, end);
		move(n - 1, temp, start, end);
	}
}

int main() 
{
	int n = 0;
	char a = 'A', b = 'B', c = 'C';
	printf("请输入有几个盘子：");
	scanf("%d", &n);
	move(n,a,b,c);
	return 0;
}