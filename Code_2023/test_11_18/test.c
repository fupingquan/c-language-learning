#define _CRT_SECURE_NO_WARNINGS 1
// function:printf函数和关键字
// author:付平权
// description:初学C语言的记录
// time:2023-11-20 17:16

#include <stdio.h>

// main函数格式标准写法
//int main()
//{
//	printf("hello C\n");
//	return 0;
//}

// 双引号里的为字符串，后面默认有‘\0’
//#include <stdio.h>
//#include <string.h>

//int main()
//{
//    char arr[] = { 'b', 'i', 't' };
//    printf("%d\n", strlen(arr));
//    return 0;
//}

//
//// 可打印字符代码展示
//# include <stdio.h>
//int main()
//{
//	int i = 0;
//	for (i = 0; i <= 127; i++)
//	{
//		printf("%c", i);
//		if (i % 16 == 15)
//			printf("\n");
//	}	
//	return 0;
//}

//字符串打印格式可以用%s，整型%d,字符用%c，双精度浮点型用%lf或%f（数字后要加f）

// C语言的32个关键字
//auto break case char const continue 
//default do double else enum extern
//float for goto if int long 
//register return short signed sizeof struct 
//switch typedef union unsigned void volatile while

//#include <stdio.h>
//
//int main()
//{
//	printf("hello esrg\n");
//	printf("hello world\n");
//
//	return 0;
//}



//int main()
//{
//	printf("%d\n", sizeof(char));
//	printf("%d\n", sizeof(short));
//	printf("%d\n", sizeof(int));
//	printf("%d\n", sizeof(long));
//	printf("%d\n", sizeof(long long));
//	printf("%d\n", sizeof(float));
//	printf("%d\n", sizeof(double));
//	printf("%d\n", sizeof(long double));
//
//	return 0;
//}


//scanf --输入数据，数据从键盘读取到内存
//printf --输出数据，数据从内存打印（输出）到屏幕

//使用库函数时候需要包含头文件<stdio.h>

//#include <stdio.h>

//int main()
//{
//	//int a = 0;
//	//scanf("%d", &a);
//	//printf("%d\n", a);
//
//	char ch = 0;
//	scanf("%c", &ch);
//	printf("%c", ch);
//
//	return 0;
//}


//int a = 2023;//全局变量
//
//void test()
//{
//	printf("2--->%d\n", a);
//}
//
//int main()
//{
//	printf("1--->%d\n", a);
//	test();
//
//	return 0;
//}

////需要声明外部符号用extern
//extern int g_val;
//int main()
//{
//	printf("%d\n", g_val);
//
//	return 0;
//}

//int main()
//{
//	int a = 3;
//	int b = 5;
//	int c = a + b;
//	printf("%d\n", c);
//
//	return 0;
//}


//int main()
//{
//	int a = 3;
//	printf("%d\n", a);
//	a = 13;//改变a的值为13
//	printf("%d\n", a);
//	return 0;
//}


//int main()
//{
	//const修饰的常变量
	//常属性
	//这里的a是具有常属性的，不能改变，但是本质上还是变量

	//const int a = 3;
	//printf("%d\n", a);
	////a = 10;不可改变
	//printf("%d\n", a);

//	//数组
//	const int n = 10;
//	/*int arr[n];
//	int arr[n] = { 0 };*/
//	return 0;
//
//}


////define定义的标识符常量，理解标识符
//#define size 10
//#define max 1000
//
////注意：后边不跟等号和分号
//int main()
//{
//	int a = size + 1;
//	int prize = max;
//	printf("%d %d\n", a, prize);
//}

//枚举常量
//可以理解为一一列举
//比如性别：男 女 
//三原色：red green blue

//enum sex
//{
//	//列出了枚举类型enum sex的所有可能取值
//	//枚举常量
//MALE = 4,FEMALE = 6
//
//};
//
//int main()
//{
//	//enum sex s = FEMALE;
//	printf("%d\n", MALE);
//	printf("%d\n", FEMALE);
//	return 0;
//}


//int main()
//{
//	////'abcdef'
//	//char ch1 = 'a';
//	//char ch2 = 'b';
//	//char ch3 = 'c';
//	//char ch4 = 'd';
//	
//	char ch[] = { 'a','b','c','d','e','f' }; //里边没有\0
//	char ch2[] = { "abcdef" };
//
//	printf("%s\n", ch);
//	printf("%s\n", ch2);
//	return 0;
//}

//#include <string.h>
//int main()
//{
//	char ch1[] = {'a','b','c'}; //里边存储的是[a][b][c]
//	char ch2[] = "abc"; //[a][b][c][\0]
//	int len1 = strlen(ch1); //计算出来的是随机值，strlen遇到\0停止
//	printf("%d\n", len1);
//
//	int len2 = strlen(ch2);
//	printf("%d\n", len2);
//	return 0;
//}

//转义字符-转变原来的意思

//int main()
//{
//	printf("are you ok\?\?");
//
//	//are you ok?? --are you ok]
//	//??)-->] 三字母词
//	return 0;
//}

//int main()
//{
//	printf("ab\ndef");
//	printf("\nc:\\test\\test.c");
//	return 0;
//}

//int  main()
//{
//	printf("%c\n", 'a');
//	printf("%c\n", 'x');
//	printf("%s\n", "abc");
//	printf("%s\n", "a");
//	printf("%s\n", "\"");
//	printf("a\\t\b");
//	printf("\a");
//	printf("abcdef\n");
//	printf("%c\n", 'f');
//	return 0;
//
//}


//int main()
//{
//	printf("%c", '\133'); //ddd
//	8进制的130 转换成10进制后得到88，把88作为ASCII值代码的字符
//	return 0;
//}

//int main()
//{
//	printf("%c\n", '\x31'); //xdd形式，dd表示2个十六进制数字
//	return 0;
//}

//#include <string.h>
//
//int main()
//{
//	printf("%d\n", strlen("c:\test\x11\328\test.c")); //15
//	//	//strlen是一个库函数
////	//计算的是\0之前出现的字符的个数
//	return 0;
//}

//int main()
//{
//	printf("加入比特就业课学习\n");
//	printf("你愿意好好学习吗?(1/0)>:");
//	int flag = 0;
//	scanf("%d", &flag);
//
//	if (flag == 1)
//		printf("好offer\n");
//	else if(flag == 0)
//		printf("卖红薯\n");
//
//	return 0;
//}


//int main()
//{
//	int line = 0;
//
//	while (line < 50000)
//	{
//		printf("敲代码: %d\n", line);
//		line++;
//	}
//
//	if (line == 50000)
//		printf("好offer\n");
//	else
//		printf("差点意思\n");
//
//	return 0;
//}