#define _CRT_SECURE_NO_WARNINGS 1
//author:付平权
//description:
//time:

#include <stdio.h>

//test()
//{
//	printf("陌生人！\n");
//	printf("加油哈！\n");
//}
//
//int main()
//{
//	int arr[10] = { 0 };
//	int i = 0;
//
//	test();
//
//	for (i = 0; i < 10; i++)
//	{
//		arr[i] = 10 - i;
//	}
//
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//
//	return 0;
//}

//代码死循环，用作调试训练
int main()
{
	int i = 0;
	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };

	for (i = 0; i < 12; i++)
	{
		arr[i] = 0;
		printf("hehe\n");
	}
	return 0;
}

//i和arr是局部变量，局部变量是放在内存中栈区上的
//栈区使用习惯：先使用高地址处的空间，再使用低地址处的空间
//又因为数组，随着下标的增长，地址是由低到高变化的

//全局变量未初始化则为1