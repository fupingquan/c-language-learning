#define _CRT_SECURE_NO_WARNINGS 1
// function:控制台扫雷游戏
// author:付平权
// description:数组的应用
// time:2023-12-09 17:16

#include "game.h"

void menu()
{
	printf("**********************\n");
	printf("*****  1.play  *******\n");
	printf("*****  0.exit  *******\n");
	printf("**********************\n");
}
void game()
{
	//完成扫雷游戏
	//mine数组中存放好雷的信息
	char mine[ROWS][COLS] = { 0 };

	//show数组中存放排查出的雷的信息
	char show[ROWS][COLS] = { 0 };

	//初始化棋盘
	InitBoard(mine, ROWS, COLS, '0');
	InitBoard(show, ROWS, COLS, '*');

	//布置雷
	SetMine(mine, ROW, COL);
	//DisplayBoard(mine, ROW, COL);

	//打印棋盘
	DisplayBoard(show, ROW, COL);

	//排查雷
	FindMine(mine, show, ROW, COL);
}

void test()
{
	int input = 0;
	srand((unsigned int)time(NULL));
	do
	{
		menu();
		printf("请选择是否开始游戏:>");
		scanf("%d", &input);
		switch (input)
		{
		case 1:
			game();
			break;
		case 0:
			printf("游戏结束，退出游戏\n");	
			break;
		default :
			printf("选择错误，请重新选择\n");
			break;
		}

	} while (input);
}

int main()
{
	test();
	return 0;
}
