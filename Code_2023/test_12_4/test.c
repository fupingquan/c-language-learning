#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>

int main()
{
	int i = 0;
	int count = 0;
	for (i = 100; i <= 200; i++)
	{
		//判断i是否是素数
		//如果是素数，就打印，不是素数就跳过
		//拿2~i-1之间的数字去挨个试除i，如果其中有一个数字整除了i，i就不是素数
		//如果所有的数字都不能整除i，i就是素数
		int j = 0;
		int flag = 1;//假设i是素数
		for (j = 2; j <= i - 1; j++)//2~8
		{
			if (i % j == 0)
			{
				flag = 0;//证明不是素数
				break;
			}
		}
		if (flag == 1)//是素数
		{
			printf("%d ", i);
			count++;
		}
	}

	printf("\ncount = %d\n", count);

	return 0;
}


