#define _CRT_SECURE_NO_WARNINGS 1
// function:二分法
// author:付平权
// description:2023年12月2日的二分法查找代码
// time:2023-12-02 16:51

# include<stdio.h>

int main()
{
	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
	int k = 7;//在数组中查找7

	int i = 0;
	int sz = sizeof(arr) / sizeof(arr[0]);
	int flag = 0;

	int left = 0;
	int right = sz-1;

	while (left <= right)
	{
		//int mid = (left + right) / 2;
		int mid = left + (right - left) / 2;
		if (arr[mid] < k)
		{
			left = mid + 1;
		}
		else if (arr[mid] > k)
		{
			right = mid - 1;
		}
		else
		{
			flag = 1;
			printf("找到了，下标是%d\n", mid);
			break;
		}
	}
	if (flag == 0)
		printf("找不到了\n");

	return 0;
}