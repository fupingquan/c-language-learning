#define _CRT_SECURE_NO_WARNINGS 1
// function:作业代码实现
// author:付平权
// description:初学C语言的记录
// time:2023-11-20 17:16

//#include <stdio.h>
////给定两个整数a和b (0 < a,b < 10,000)，计算a除以b的整数商和余数。
//int main()
//{
//    int a = 0;
//    int b = 0;
//    scanf("%d %d", &a, &b);
//    int m = a / b;
//    int n = a % b;
//    printf("%d %d\n", m, n);//商 余数
//
//    return 0;
//}

//# include<stdio.h>
////据说智商140以上者称为天才，KiKi想知道他自己是不是天才，请帮他编程判断。
//// 输入一个整数表示一个人的智商，如果大于等于140，则表明他是一个天才，输出“Genius”。
//int main()
//{
//	int n = 0;
//	while (scanf("%d", &n) != EOF)
//	{
//		if (n >= 140)
//		{
//			printf("Genius");
//		}
//	}
//	return 0;
//}

//# include <stdio.h>
////判断2个数的大小
//int main()
//{
//	int num1 = 0;
//	int num2 = 0;
//	while (scanf("%d%d", &num1, &num2) != EOF)
//	{
//		if (num1 > num2)
//		{
//			printf("%d>%d\n", num1, num2);
//		}
//		else if (num1 < num2)
//		{
//			printf("%d<%d\n", num1, num2);
//		}
//		else
//		{
//			printf("%d=%d\n", num1, num2);
//		}
//	}
//	return 0;
//}


//# include <stdio.h>
////判断一个整数是否能5整除
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	if (n % 5 == 0)
//	{
//		printf("YES\n");
//	}
//	else
//	{
//		printf("NO\n");
//	}
//	return 0;
//}

//# include <stdio.h>
////打印用“*”组成的线段图案。
//int main()
//{
//    int n = 0;
//    while (scanf("%d", &n) != EOF)
//    {
//        int i = 0;
//        for (i = 0; i < n; i++)
//        {
//            printf("*");
//        }
//        printf("\n");
//    }
//	return 0;
//}

//# include <stdio.h>
////打印1-100之间所有3的倍数的数字
//int main()
//{
//	int i = 0;
//	for (i = 0; i <=100; i++)
//	{
//		if (i % 3 == 0)
//		{
//			printf("%d ", i);
//		}
//	}
//	return 0;
//}

//#include <stdio.h>
////写代码将三个整数数按从大到小输出
//int main()
//{
//	int a = 2;
//	int b = 3;
//	int c = 1;
//	scanf("%d%d%d", &a, &b, &c);
//	if (a < b)
//	{
//		int tmp = 0;
//		a = b;
//		b = tmp;
//	}
//	if (a < c)
//	{
//		int tmp = a;
//		a = c;
//		c = tmp;
//	}
//	if (b < c)
//	{
//		int tmp = b;
//		b = c;
//		c = tmp;
//	}
//	printf("a=%d b=%d c=%d\n", a, b, c);
//	return 0;
//}


//# include <stdio.h>
////已经给出的三条边a，b，c能否构成三角形，如果能构成三角形，判断三角形的类型（等边三角形、等腰三角形或普通三角形）
//int main()
//{
//	int a = 0;
//	int b = 0;
//	int c = 0;
//	while (~scanf("%d %d %d", &a, &b, &c))
//	{
//		if ((a + b > c) && (a + c > b) && (b + c > a))
//		{
//			//三角形
//			if (a == b && b == c) //等边三角形
//			{
//				printf("Equilateral triangle!\n");
//			}
//			else if (((a == b) && (b != c)) || ((a == c) && (c != b)) || ((b == c) && (c != a))) //等腰
//			{
//				printf("Isosceles triangle!\n");
//			}
//			else
//			{
//				printf("Ordinary triangle!\n");
//			}
//		}
//		else 
//		{
//			//不是三角形
//			printf("Not a triangle!\n");
//		}
//	}
//	return 0;
//}


//# include<stdio.h>
////计算1/1-1/2+1/3-1/4+1/5 …… + 1/99 - 1/100 的值，打印出结果
//int main()
//{
//	int i = 0;
//	double sum = 0.0;
//	int flag = 1;
//	for (i =1; i <= 100; i++)
//	{
//		sum+=flag*1.0 / i;
//		flag = -flag;
//	}
//	printf("%lf\n", sum);
//	return 0;
//}

//# include <stdio.h>
////数一下 1到 100 的所有整数中出现多少个数字9
//int main()
//{
//	int i = 0;
//	int count = 0;
//	for (i = 1; i <= 100; i++)
//	{
//		if (i % 10 == 9)
//			count++;
//		if (i / 10 == 9)
//			count++;
//	}
//	printf("%d\n", count);
//	return 0;
//}


//# include <stdio.h>
//
//int main()
//{
//	int year = 0;
//	for (year = 1000; year < 2000; year++)
//	{
//		//判断year是否为闰年
//		if (year % 4 == 0)
//		{
//			if (year % 100 != 0)
//			{
//				printf("%d ", year);
//			}
//		}
//		if (year % 400 == 0)
//		{
//			printf("%d ", year); //每400年在闰一次
//		}
//	}
//	return 0;
//}

//# include<stdio.h>
//int main()
//{
//	int year = 0;
//	for (year = 1000; year <= 2000; year++)
//	{
//		if (((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0)) //另一种写法
//		{
//			printf("%d ", year);
//		}
//	}
//
//	return 0;
//}



# include<stdio.h>
//求10 个整数中最大值
int main()
{
	int arr[10] = { 0 };
	int i = 0;
	int max = 0;
	for (i = 0; i < 10; i++)
	{
		scanf("%d", &arr[i]);
	}
	max = arr[0]; //..
	for (i = 1; i < 10; i++)
	{
		if (arr[i] > max)
			max = arr[i];
	}
	printf("max=%d\n", max);
	return 0;
}