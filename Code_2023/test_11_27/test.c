#define _CRT_SECURE_NO_WARNINGS 1
// function:作业题中中的代码段
// author:付平权
// description:学习C语言中的代码段
// time:2023-11-25 17:42

//#include <stdio.h>
//
//int main()
//{
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		if (i = 5)
//			printf("%d ", i);
//	}
//	return 0;
//}

//# include <stdio.h>
//
//int func(int a)
//{
//    int b;
//    switch (a)
//    {
//    case 1: b = 30;
//    case 2: b = 20;
//    case 3: b = 16;
//    default: b = 0;
//    }
//    return b;
//}



//#include <stdio.h>
//int main() {
//	int x = 3;
//	int y = 3;
//	switch (x % 2) {
//	case 1:
//		switch (y)
//		{
//		case 0:
//			printf("first");
//		case 1:
//			printf("second");
//			break;
//		default: printf("hello");
//		}
//	case 2:
//		printf("third");
//	}
//	return 0;
//}


//# include <stdio.h>
//int main()
//{
//	int x = 12;
//	int y = 15;
//	int z = (x || (y -= x));
//	printf("%d %d\n", y, z);
//	return 0;
//}


//# include <stdio.h>
//
//int main()
//{
//	//int a = 0;
//	int i = 0;
//	for (i = 3; i < 100; i += 3)
//	{
//		printf("%d ", i);
//	}
//	return 0;
//}


//# include <stdio.h>
//
//int main()
//{
//	int a = 0;
//	scanf("%d", &a);
//	while (a)
//	{
//		printf("%d ", a%10);
//		a = a / 10;
//		
//	}
//	return 0;
//}



//#include <stdio.h>
//
//int main() {
//    int a = 1;
//    int b = 1;
//    int c = 1;
//    while (scanf("%d %d %d", &a, &b, &c) != EOF)
//    {
//        if (a + b > c && a + c > b && b + c > a)
//        {
//            if (a == b && b == c)
//            {
//                printf("Equilateral triangle!\n");
//            }
//            else if (a == b || a == c || b == c)
//            {
//                printf("Isosceles triangle!\n");
//            }
//            else
//            {
//                printf("Ordinary triangle!\n");
//            }
//        }
//        else
//        {
//            printf("Not a triangle!\n");
//        }
//    }
//    return 0;
//}


//#include<stdio.h>
//int main()
//{
//	double sum = 0.0;
//	int flag = 1;
//	int i;
//	for (int i = 1; i <= 100; i++)
//	{
//		sum += (flag) * 1.0 / i;
//		flag = -flag;
//	}
//	printf("%f\n", sum);
//	return 0;
//}


//#include<stdio.h>
//
//void main()
//{
//	int count = 0;
//	int i = 0;
//	for (i = 0; i <= 100; i++) 
//	{
//		if (i % 10 == 9 || i / 10 == 9)//1到 100 的所有整数中出现9的个数
//			count++;
//	}
//	printf("1到 100 的所有整数中出现%d个数字9\n", count);
//}

# include<stdio.h>

//int main()
//{
//	int year = 0;
//	for (year = 1000; year < 2000; year++)//打印1000年到2000年之间的闰年
//	{
//		if (year % 4 == 0 && year % 100 != 0)
//			printf("%d ",year);
//		else if (year%400==0)
//		{
//			printf("%d ",year);
//		}
//	}
//	return 0;
//}


# include <stdio.h>

int main(void)
{
	int a[10];
	int i;
	int b = 0;
	printf("输入十个数\n"); 
	for (i = 0; i < 10; i++) //求十个数中的最大值
	{
		scanf("%d", &a[i]);

		if (b < a[i])
		{
			b = a[i];
		}
	}
	printf("最大值%d\n", b);
	return 0;

}