#define _CRT_SECURE_NO_WARNINGS 1
// function:数据类型的范围
// author:付平权
// description:初学C语言的记录，数据类型的范围
// time:2023-12-05 17:19

#include<stdio.h>
//#include<limits.h>


//int main()
//{
//	int a = 1;
//	int b = ++a;
//	printf("a=%d b=%d\n", a, b);
//	return 0;
//}


//int sum(int a)
//{
//	int c = 0;
//	static int b = 3;
//	c += 1;
//	b += 2;
//	return (a+b+c);
//}
//
//int main()
//{
//	int i;
//	int a = 2;
//	for (i = 0; i < 5;i++)
//	{
//		printf("%d,",sum(a));
//	}
//	//return 0;
//}


//int main()
//{
//    char str[] = "hello bit";
//    printf("%zd %d\n", sizeof(str), strlen(str));//计算长度遇到\0结束
//    return 0;
//}

#include <stdio.h>
#include <time.h>
#include <stdlib.h>
void menu()
{
	printf("***********************\n");
	printf("***     1. play     ***\n");
	printf("***     0. exit     ***\n");
	printf("***********************\n");
}

void game()
{
	int guess = 0;
	//1. 生成随机数
	int ret = rand() % 100 + 1;
	//printf("%d\n", ret);
	//2. 猜数字
	int count = 5;
	while (count)
	{
		printf("你还有 %d 次机会\n", count);
		printf("请输入要猜的数字:>");
		scanf("%d", &guess);
		if (guess < ret)
		{
			printf("猜小了\n");
		}
		else if (guess > ret)
		{
			printf("猜大了\n");
		}
		else
		{
			printf("恭喜你，猜对了，数字是:%d\n", ret);
			break;
		}
		count--;
	}
	if (count == 0)
	{
		printf("很遗憾，五次机会使用完，挑战失败，随机数是：%d\n", ret);
	}
}

//猜数字游戏
int main()
{
	int input = 0;
	srand((unsigned int)time(NULL));
	do
	{
		menu();
		printf("请选择:>");
		scanf("%d", &input);//1 0 
		switch (input)
		{
		case 1:
			game();
			break;
		case 0:
			printf("退出游戏\n");
			break;
		default:
			printf("选择错误，重新选择\n");
			break;
		}
		//...
	} while (input);

	return 0;
}



//  1  2  6
//求1!+2!+3!+4!+...10!的和
//阶乘
//5! = 5*4*3*2*1
//
//n!
//

//int main()
//{
//	int n = 0;
//	int i = 0;
//	int ret = 1;
//	int sum = 0;
//
//	for (n = 1; n <= 3; n++)
//	{
//		ret = 1;
//		for (i = 1; i <= n; i++)
//		{
//			ret *= i;
//		}
//		sum += ret;
//	}
//	
//	printf("%d\n", sum);
//
//	return 0;
//}

//照着程序走
//看一看程序有没有按照我们预期的方式在走
//
//如果没有按照预期走，那么说明代码有问题！


//int main()
//{
//	int n = 0;
//	int i = 0;
//	int ret = 1;
//	int sum = 0;
//
//	for (n = 1; n <= 3; n++)
//	{
//		ret *= n;
//		sum += ret;
//	}
//
//	printf("%d\n", sum);
//
//	return 0;
//}
//


//#include <stdio.h>

//int main()
//{
//    int i = 0;
//
//    int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };//0~9
//    printf("&i = %p\n", &i);
//    printf("arr = %p\n", arr);
//
//    for (i = 0; i <= 12; i++)
//    {
//        arr[i] = 0;
//        printf("hehe\n");
//    }
//
//    return 0;
//}

//void test(int arr[])
//{
//	//...
//}
//
//void test2(int arr2[3][5])
//{
//
//}
//int main()
//{
//	//int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	//test(arr);
//
//	int arr2[3][5] = { {1,2,3}, {2,3,4,5,6},{1,3,5,7,9} };
//
//	test2(arr2);
//	return 0;
//}




//
//#include <stdio.h>
//
//
//int main()
//{
//	printf("hehe\n");
//	return 0;
//}

//
//int Add(int x, int y)
//{
//	return x + y;
//}
//
//int main()
//{
//	int r = Add(3, 5);
//	return 0;
//}
//
//



