#define _CRT_SECURE_NO_WARNINGS 1
// function:最大公约数和素数
// author:付平权
// description:2023年12月1日代码--最大公约数和素数
// time:2023-12-1 12:16

//# include<stdio.h>
//
////辗转相除法
//int main() {
//	int a;
//	int b;
//	printf("请输入两个正整数：");
//	scanf("%d %d", &a, &b);
//	int k = 0;
//	while (k = a % b) {
//		a = b;
//		b = k;
//	}
//	printf("最大公约数为:%d\n", b);
//	return 0;
//}

#include <stdio.h>
//int main()
//{
//	int i = 0;
//	int count = 0;
//	for (i = 100; i <= 200; i++)
//	{
//		//判断i是否是素数
//		//如果是素数，就打印，不是素数就跳过
//		//拿2~i-1之间的数字去挨个试除i，如果其中有一个数字整除了i，i就不是素数
//		//如果所有的数字都不能整除i，i就是素数
//		int j = 0;
//		int flag = 1;//假设i是素数
//		for (j = 2; j <= i - 1; j++)//2~8
//		{
//			if (i % j == 0)
//			{
//				flag = 0;//证明不是素数
//				break;
//			}
//		}
//		if (flag == 1)//是素数
//		{
//			printf("%d ", i);
//			count++;
//		}
//	}
//
//	printf("\n素数count = %d\n", count);
//
//	return 0;
//}


#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
int main()
{
	int x = 0;
	int i = 0;
	unsigned int count = 0;
	x = 2;
	printf("%d ", x);
	for (x = 3; x < 1000; x += 2)  //只产生奇数
	{
		for (i = 3; i < x; i += 2)  //只产生奇数
		{
			count++;
			if (x % i == 0)
			{
				break;
			}
		}
		if (x == i)
		{
			printf("%d ", x);
		}
	}
	printf("\n\n\n");
	printf("运算的次数：%d ", count);
	return 0;
}
