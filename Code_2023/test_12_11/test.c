#define _CRT_SECURE_NO_WARNINGS 1
//author:付平权
//description:
//time:

//#include "game.h"
//
//
//void menu()
//{
//	printf("**********************\n");
//	printf("***** 1. play    *****\n");
//	printf("***** 0. exit    *****\n");
//	printf("**********************\n");
//}
//
//void game()
//{
//	//完成扫雷游戏
//	//mine数组中存放布置好的雷的信息
//	char mine[ROWS][COLS] = { 0 };//数组全部初始化为'0'
//
//	//show数组中存放排查出的雷的信息
//	char show[ROWS][COLS] = { 0 };//数组全部初始化为'*'
//
//	//初始化棋盘
//	InitBoard(mine, ROWS, COLS, '0');
//	InitBoard(show, ROWS, COLS, '*');
//
//	//布置雷
//	//就9*9的棋盘上随机布置10个雷
//	SetMine(mine, ROW, COL);
//	//DisplayBoard(mine, ROW, COL);
//
//	//打印棋盘
//	DisplayBoard(show, ROW, COL);
//
//	//排查雷
//	FindMine(mine, show, ROW, COL);
//}
//
//void test()
//{
//	int input = 0;
//	srand((unsigned int)time(NULL));
//	do
//	{
//		menu();
//		printf("请选择:>");
//		scanf("%d", &input);//1 0 x
//		switch (input)
//		{
//		case 1:
//			game();
//			break;
//		case 0:
//			printf("游戏结束，退出游戏\n");
//			break;
//		default:
//			printf("选择错误，重新选择\n");
//			break;
//		}
//
//	} while (input);
//}
//
//int main()
//{
//	test();
//	return 0;
//}



void menu()
{
	printf("***********************\n");
	printf("***     1. play     ***\n");
	printf("***     0. exit     ***\n");
	printf("***********************\n");
}

void game()
{
	int guess = 0;
	//1. 生成随机数
	int ret = rand() % 100 + 1;
	//printf("%d\n", ret);
	//2. 猜数字
	int count = 5;
	while (count)
	{
		printf("你还有 %d 次机会\n", count);
		printf("请输入要猜的数字:>");
		scanf("%d", &guess);
		if (guess < ret)
		{
			printf("猜小了\n");
		}
		else if (guess > ret)
		{
			printf("猜大了\n");
		}
		else
		{
			printf("恭喜你，猜对了，数字是:%d\n", ret);
			break;
		}
		count--;
	}
	if (count == 0)
	{
		printf("很遗憾，五次机会使用完，挑战失败，随机数是：%d\n", ret);
	}
}

int main()
{
	int input = 0;
	srand((unsigned int)time(NULL));
	do
	{
		menu();
		printf("请选择:>");
		scanf("%d", &input);//1 0 
		switch (input)
		{
		case 1:
			game();
			break;
		case 0:
			printf("退出游戏\n");
			break;
		default:
			printf("选择错误，重新选择\n");
			break;
		}
		//...
	} while (input);

	return 0;
}