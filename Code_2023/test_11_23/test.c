﻿#define _CRT_SECURE_NO_WARNINGS 1
// function:作业题中的代码段
// author:付平权
// description:学习C语言中的代码段
// time:2023-11-25 12:31

//# include <stdio.h>
//问题代码
//int main()
//{
//	int a, b, c;
//	a = 5;
//	c = ++a;
//	b = ++c, c++, ++a, a++;
//	b += a++ + c;
//	printf("a=%d b=%d c=%d\n", a, b, c);
//	return 0;
//}


//#include <stdio.h>
//
//int main()
//{
//	int a = 7;
//	int b = 2;
//	float c = 2.0f;
//	printf("%d\n", 3);
//	printf("%f\n", a / c); //希望得到浮点数的结果，两个运算数必须⾄少有⼀个浮点数，这时 C 语⾔就会进⾏浮点数除法。
//
//	return 0;
//}

//# include <stdio.h>
//
//int main()
//{
//	printf("%5d\n", 123456);//%5d 表⽰这个占位符的宽度⾄少为5位
//	return 0;
//}


//#include<stdio.h>
//int main()
//{
//    int n;
//    scanf("%d", &n);
//    if (n >= 140)
//        printf("Genius");
//    return 0;
//}


//#include <stdio.h>
//int main()
//{
//    int a = 1;
//    int b = 1;
//    while (scanf("%d %d", &a, &b) != EOF)
//    {
//
//        if (a == b)
//        {
//            printf("%d=%d\n", a, b);
//        }
//        else if (a > b)
//        {
//            printf("%d>%d\n", a, b);
//        }
//        else
//            printf("%d<%d\n", a, b);
//    }
//
//    return 0;
//}



#include<stdio.h>
//输入数字，打印星星
int main()
{
    int a, i;
    while (scanf("%d", &a) != EOF)
    {
        for (i = 0; i < a; i++)
        {
            printf("*"); //打印小星星
        }
        printf("\n");
    }
    return 0;
}