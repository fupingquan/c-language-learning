#define _CRT_SECURE_NO_WARNINGS 1
//author:付平权
//description:
//time:

#include <stdio.h>

int main()
{

}


#if 0 //这不是注释，它是条件编译

//计算1到n的和 写法一
int main()
{
	int n = 0;
	scanf("%d", &n);
	int i = 0;
	long long sum = 0;
	for (i = 0; i <=n; i++)
	{
		sum += i;
	}
	printf("%lld\n", sum);
	return 0;
}

int main()  //写法二
{
	long long n = 0;
	scanf("%lld", &n);
	long long sum = 0;
	sum = (1 + n) * n / 2;
	printf("%lld\n", sum);
	return 0;
}


int main()
{
	int i = 0;
	int count = 0;
	for (i = 1; i < +100; i++)
	{
		if (i % 10 == 9) //个位是9
			count++;
		if (i / 10 == 9)  //十位是9
			count++;
	}
	printf("1到100中9的个数为：%d\n", count);
	return 0;
}

//9 19 29 39 49 59 69 79 89 99
//90 91 92 93 94 95 96 97 98 
//错误写法
int main()
{
	int i = 0;
	int count = 0;
	for (i = 1; i < +100; i++)
	{
		if (i % 10 == 9) //个位是9
			count++;
		else if (i / 10 == 9)  //十位是9
			count++;
	}
	printf("1到100中9的个数为：%d\n", count);
	return 0;
}


// 1 -1/2 1/3 -1/4···1/n，每一项加起来
int main()
{
	int i = 0;
	double sum = 0;
	int flag = 1;
	for (i = 1; i <= 100; i++)
	{
		sum += flag*1.0 / i;  //两端至少有一个浮点数才能执行浮点数除法
		flag = -flag;
	}
	printf("%lf\n", sum);

	return 0;
}



//求10个整数中最大值
int main()
{
	int arr[10] = { 0 };
	//输入
	int i = 0;
	int sz = sizeof(arr) / sizeof(arr[0]);
	for (i = 0; i < sz; i++)
	{
		scanf("%d", &arr[i]);
	}
	//求最大值
	int max = arr[0];
	//int max = 0;//错误写法，假设的最大值要是数组里的某个
	for (i = 0; i < sz; i++)
	{
		if (arr[i] > max)
		{
			max = arr[i];
		}
	}
	//输出
	printf("10个数中最大的值是：%d\n", max);

	return 0;
}



////九九乘法表
//int main()
//{
//	int i = 0;
//	for (i = 1; i <= 9; i++)
//	{
//		int j = 0;
//		for (j = 1; j <= i; j++)
//		{
//			printf("%d*%d=%-2d", j, i, i * j);
//		}
//		printf("\n");
//	}
//	return 0;
//}

#endif