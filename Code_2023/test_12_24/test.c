#define _CRT_SECURE_NO_WARNINGS 1
//author:付平权
//description:
//time:

//九九乘法表  (小99)
#include <stdio.h>
int main()
{
	for (int i = 1; i <= 9; i++)
	{
		for (int j = 1; j <= i; j++)
		{
			printf("%d*%d=%2d ", j, i, j * i);
		}
		printf("\n");
	}

	return 0;
}