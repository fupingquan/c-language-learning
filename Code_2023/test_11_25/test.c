#define _CRT_SECURE_NO_WARNINGS 1
// function:作业题中中的代码段
// author:付平权
// description:学习C语言中的代码段
// time:2023-11-25 12:32

#include <stdio.h>

//例子：输入一个整数，判断是否为奇数
//int main()
//{
//	int num = 0;
//	//输入
//	scanf("%d", &num);
//	//判断
//	if (num % 2 == 1)
//		printf("奇数\n");
//
//	return 0;
//}


//int main()
//{
//	int num = 0;
//	//输入
//	scanf("%d", &num);
//	//判断
//	if (num % 2 == 1)
//		printf("奇数\n");
//	else
//		printf("偶数\n");
//
//	return 0;
//}


//输入一个年龄， >= 18岁就输出：成年，否则就输出：未成年

//int main()
//{
//	int age = 0;
//	//输入
//	scanf("%d", &age);
//	if (age >= 18)
//	{
//		printf("成年\n");
//		printf("可以恋爱了\n");
//	}	
//	else
//	{
//		printf("未成年\n");
//		printf("不能谈恋爱\n");
//	}
//
//	return 0;
//}

int Add(int x, int y)
{
	int z = 0;
	z = x + y;
	return z;
}

int main()
{
	int a = 10;
	int b = 20;
	int ret =Add(a, b);
	printf("%d", ret);
	return 0;
}

//%p - 专门用来打印地址的，是16进制的形式表示地址的
//
//int main()
//{
//	int a = 15;//虽然a占有4个字节，但是当我们&a的时候，拿到的是4个字节中第一个字节的地址
//	//printf("%p\n", &a);
//
//	int * pa = &a;//pa存放a的地址的，是一个变量，叫：指针变量，意思是存放指针的变量
//	*pa = 30;//解引用操作符，*pa 就是a
//	
//	printf("%d\n", a);
//	return 0;
//}

//指针 - 地址 -编号
//指针变量 - 变量 - 存放地址的变量

//口头语中说的指针，基本上都是指针变量

//
//int main()
//{
//	double d = 3.14;
//	double* pd = &d;
//	*pd = 0;
//	printf("%lf\n", d);
//	return 0;
//}
//