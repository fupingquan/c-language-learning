// 1. 常用的库
#include <stdio.h>    // 标准输入输出库
#include <stdlib.h>   // 标准库
#include <string.h>   // 字符串处理库
#include <math.h>     // 数学函数库
#include <time.h>     // 时间相关库

// 2. printf基本用法
#include <stdio.h>    // 引入stdio.h这个头文件

int main()
{
    printf("Hello World\n");    
    // 输出字符串，双引号里面的称为字符串，详细后边会讲

    printf("我的年龄是%d岁\n", 25);   
    // 输出整数，%d为占位符，该语句即把25换到%d

    return 0;
}