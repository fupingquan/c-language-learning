// 讲解注释用

// 这是一个单行注释

/* 这是多行注释第一行
   这是多行注释第二行
   ...
*/

//int main()
//{
//    // 程序代码
//    return 0;
//}

int main(int argc, char* argv[])
{
    // 程序代码
    return 0;
}