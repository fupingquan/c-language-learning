// 语句分类讲解

// 该文件不可直接运行

int x = 10;        // 赋值语句
printf("Hello");   // 函数调用语句



{
    int x = 10;
    int y = 20;
    printf("x = %d, y = %d", x, y);
}



// if语句
if (x > 0) {
    printf("x is positive");
}

// if-else语句
if (x > 0) {
    printf("x is positive");
}
else {
    printf("x is not positive");
}



// switch语句
switch (x) {
case 1:
    printf("x is 1");
    break;
case 2:
    printf("x is 2");
    break;
default:
    printf("x is neither 1 nor 2");
}



// while循环
while (x < 10) {
    printf("%d", x);
    x++;
}

// do-while循环
do {
    printf("%d", x);
    x++;
} while (x < 10);

// for循环
for (int i = 0; i < 10; i++) {
    printf("%d", i);
}



// break语句
for (int i = 0; i < 10; i++) {
    if (i == 5) {
        break;  // 退出循环
    }
    printf("%d", i);
}



// continue语句
for (int i = 0; i < 10; i++) {
    if (i % 2 == 0) {
        continue;  // 跳过本次循环
    }
    printf("%d", i);
}



// return语句
int sum(int a, int b) {
    return a + b;  // 返回函数结果
}



;  // 空语句