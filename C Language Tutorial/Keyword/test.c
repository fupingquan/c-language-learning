// C语言关键字


// 该文件不支持运行


/* 数据类型 */
int: 整型
float : 单精度浮点型
double : 双精度浮点型
char : 字符型
void : 空类型
short : 短整型
long : 长整型
signed : 有符号
unsigned : 无符号

/* 控制流程关键字 */
if : 条件判断
else : 条件判断否定分支
switch : 开关语句
case: switch语句中的情况标记
default: switch语句中的默认情况
for : for循环
while : while循环
do : do - while循环
break : 跳出循环
continue : 继续下一次循环
goto : 无条件跳转
return : 函数返回

/* 存储类关键字 */
auto : 自动存储类型
static: 静态存储类型
register : 寄存器存储类型
extern : 外部变量声明

/* 结构相关关键字 */
struct : 结构体
union : 联合体
enum : 枚举类型
typedef : 类型定义

/* 其他关键字 */
const：常量声明
volatile : 易变变量声明
sizeof : 获取数据类型大小

/* 注意：
1. 不同版本的C语言标准可能包含不同的关键字。
2. 某些编译器可能包含额外的关键字。
*/