#define _CRT_SECURE_NO_WARNINGS 1
//author:付平权
//description:
//time:

//函数返回的是目标空间的起始地址
//const 修饰指针的时候
//当const 放在*的左边的时候，限制的是指针指向的内容，不能通过指针变量改变指针指向的内容，但是指针变量的本身是可以改变的
//当const 放在*的右边的时候，限制的是指针变量本身，指针变量的本身是不能改变的，但是指针指向的内容是可以通过指针来改变的
//
//char* my_strcpy(char* dest, const char * src)
//{
//	char* ret = dest;
//	//断言
//	assert(dest != NULL);
//	assert(src != NULL);
//
//	while (*dest++ = *src++) 
//		;//空语句
//
//	return ret;
//}
//
//
//int main()
//{
//	char arr1[] = "hello bit";
//	char arr2[20] = "xxxxxxxxxxxxx";
//	char* p = NULL;
//
//	//my_strcpy(arr2, arr1);
//	//printf("%s\n", arr2);
//
//	printf("%s\n", my_strcpy(arr2, arr1));
//
//	return 0;
//}

//int main()
//{
//	int a = 10;
//	int* pa = &a;
//	*pa = 20;
//	printf("%d\n", a);
//	return 0;
//}

//野指针
//int main()
//{
//	int* p;//局部变量不初始化的时候，内容是随机值
//	*p = 20;
//	printf("%d\n", *p);
//
//	return 0;
//}
//
