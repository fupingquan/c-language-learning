#define _CRT_SECURE_NO_WARNINGS 1
//author:付平权
//description:
//time:

#include <stdio.h>

//int main()
//{
//	int a = 0x11223344;
//	//当前环境小端字节序存放
//	return 0;
//}

//int main()
//{
//	int a = 1;
//	char* p = (char*)&a;  //int*
//	if (*p == 1)
//		printf("小端\n");
//	else
//		printf("大端\n");
//	return 0;
//}

//int check_sys()
//{
//	int i = 1;
//	return (*(char*)&i);
//}
//
//int main()
//{
//	int ret = check_sys();
//	if (ret == 1)
//		printf("小端\n");
//	else
//		printf("大端\n");
//	return 0;
//}


//int arr[10]
//int* p = arr;
//*(p+i) == arr[i]
//
//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	int i = 0;
//	int *p = arr;
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", arr[i]);//[] 操作符
//		printf("%d ", *(p+i));//[] 操作符
//	}
//
//	//2+3 --> 3+2
//	//arr[i] --> i[arr]
//	return 0;
//}
//

//地址-地址
//指针-指针

//int main()
//{
//	int arr[10] = { 0 };
//	printf("%d\n", &arr[9] - &arr[0]);//?
//	printf("%d\n", &arr[0] - &arr[9]);//?
//
//	return 0;
//}

//
//int main()
//{
//	int arr[10] = { 0 };
//	char ch[5] = {0};
//	//指针和指针相减的前提是：两个指针指向了同一块空间
//	printf("%d\n", &ch[4] - &arr[0]);//err
//
//	return 0;
//}
//

//int my_strlen(char* s) 
//{
//	int count = 0;
//	while (*s != '\0')
//	{
//		count++;
//		s++;
//	}
//	return count;
//}
//
//int my_strlen(char* s)
//{
//	if (*s == '\0')
//		return 0;
//	else
//		return 1 + my_strlen(s + 1);
//}

//int my_strlen(char* s) 
//{
//	char* start = s;
//	while (*s != '\0')
//	{
//		s++;
//	}
//	return s - start;
//}
//
//int main()
//{
//	char arr[] = "abcdef";
//	int len = my_strlen(arr);
//	printf("%d\n", len);
//
//	return 0;
//}

//int my_strlen(char* s)
//{
//	char* start = s;
//	while (*s)//a b c d e f \0->0
//	{
//		s++;
//	}
//	return s - start;
//}
//
//int my_strlen(char* s)
//{
//	char* start = s;
//	while (*s++);
//	return s - start -1;
//}
//
//int main()
//{
//	char arr[] = "abcdef";
//	int len = my_strlen(arr);
//	printf("%d\n", len);
//
//	return 0;
//}
//