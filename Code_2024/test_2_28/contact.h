#pragma once

#include <stdio.h>
#include <assert.h>

#define MAX 100
#define MAX_NAME 20
#define MAX_SEX 5
#define MAX_TELE 12
#define MAX_ADDR 30


//类型的声明

typedef struct PeoInfo
{
	char name[MAX_NAME];  //名字
	int age;        //年龄
	char sex[MAX_SEX];   //性别
	char tele[MAX_TELE];  //电话
	char addr[MAX_ADDR];  //地址
}PeoInfo;


//通讯录
// 静态版本

typedef struct Contact
{
	PeoInfo data[MAX];
	int sz;  //统计已经存了几个人的信息
}Contact;


//函数声明

//初始化信息
void InitContact(Contact* pc);

//增加联系人
void ADDContact(Contact* pc);