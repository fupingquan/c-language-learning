#define _CRT_SECURE_NO_WARNINGS 1
//author:付平权

#include "contact.h"


void menu()
{
	printf("--------------------------------\n");
	printf("|***** 1.Add     2. Del   *****|\n");
	printf("|***** 3.Search  4.Modify *****|\n");
	printf("|***** 5.Show    6.sort   *****|\n");
	printf("|***** 0.Exit             *****|\n");
	printf("--------------------------------\n");
}

void test()
{
	int input = 0;
	//首先得有通讯录
	Contact con;  //con结构体变量
	InitContact(&con);
	do
	{
		menu();
		printf("请选择:>");
		scanf("%d", &input);
		switch (input)
		{
		case 1:
			AddContact(&con);
			break;
		case 2:
			DelContact(&con);
			break;
		case 3:
			SearchContact(&con);
			break;
		case 4:
			ModifyContact(&con);
			break;
		case 5:
			ShowContact(&con);
			break;
		case 0:
			printf("退出游戏\n");
			break;
		default:
			break;
		}
	} while (input);
}
int main()
{
	test();
	return 0;
}