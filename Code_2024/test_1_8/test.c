#define _CRT_SECURE_NO_WARNINGS 1
//author:付平权
//description:
//time:

//1.整型提升
//2.算数转换

//问题代码
//a*b+c*d+e*f
 
//c+ --c;

//非法代码,无法确定唯一的计算路径
#include <stdio.h>
//int main()
//{
//	int i = 10;
//	i = i-- - --i * (i = -3) * i++ + ++i;
//	printf("i=%d\n", i);
//	return 0;
//}

////错误代码
//int fun()
//{
//	static int count = 1;
//	return ++count;
//}
//
//int main()
//{
//	int answer;
//	answer = fun() - fun() * fun();
//	printf("%d\n", answer);//VS2022输出为-10
//	return 0;
//}


////问题代码
//int main()
//{
//	int i = 1;
//	int ret = (++i) + (++i) + (++i);
//
//	printf("%d\n", ret);  //12
//	printf("%d\n", i);    //4
//	return 0;
//}

//将数组A中的内容和数组B中的内容进行交换。（数组一样大）

//
//int main()
//{
//	int A[5] = { 1,3,5,7,9 };
//	int B[5] = { 2,4,6,8,0 };
//
//	int tmp = 0;
//	int i = 0;
//	int sz = sizeof(A) / sizeof(A[0]);
//	for (i = 0; i < sz; i++)
//	{
//		tmp = A[i];
//		A[i] = B[i];
//		B[i] = tmp;
//	}
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", A[i]);
//	}
//	printf("\n");
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", B[i]);
//	}
//	printf("\n");
//	//int C[5] = {0};
//	//数组名是首元素的地址 - 常量的值
//	//0x0012ff40
//
//	//err
//	/*C = A;
//	A = B;
//	B = C*/;
//
//	return 0;
//}


// 创建一个整形数组，完成对数组的操作
// 
//实现函数init() 初始化数组为全0
//实现print()  打印数组的每个元素
//实现reverse()  函数完成数组元素的逆置。
//要求：自己设计以上函数的参数，返回值。


void print(int arr[], int sz)
{
	int i = 0;
	for (i = 0; i < sz; i++)
	{
		printf("%d ", arr[i]);
	}
	printf("\n");
}

void reverse(int arr[], int sz)
{
	int left = 0;
	int right = sz - 1;

	while (left < right)
	{
		int tmp = arr[left];
		arr[left] = arr[right];
		arr[right] = tmp;
		left++;
		right--;
	}
}

void init(int arr[], int sz)
{
	int i = 0;
	for (i = 0; i < sz; i++)
	{
		arr[i] = 0;
	}
}

int main()
{
	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	print(arr, sz);
	//逆置
	reverse(arr, sz);
	print(arr, sz);
	init(arr, sz);
	print(arr, sz);
	return 0;
}