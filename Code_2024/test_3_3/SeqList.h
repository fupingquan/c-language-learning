#pragma once

#include <stdio.h>
#include <stdlib.h>

//#define N 1000
//
////静态链表
//typedef int SLDatatype;
//struct SeqList
//{
//	SLDatatype a[N];
//	int size;
//};


//动态顺序表
//typedef double SLADatatype;
typedef int SLDatatype;
typedef struct SeqList
{
	SLDatatype* a;
	int size;     //存储的有效数据个数
	int capacity; //容量
}SL;

//初始化
void SLIint(SL* ps1);

//删除
void SLDestroy(SL* Ps1);

//打印
void SLPrint(SL* ps1);

//后插
void SLPushback(SL* ps1, SLDatatype x);
//void SLPushFront(SL* psl, SLDatatype x);
//void SLPopBack(SL* psl);
//void SLPopFront(SL* psl);