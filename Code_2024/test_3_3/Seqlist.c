#define _CRT_SECURE_NO_WARNINGS 1
//author:付平权

#include "SeqList.h"

void SLIint(SL* ps1)
{
	ps1->a = (SLDatatype*)malloc(sizeof(SLDatatype) * 4);
	if (ps1->a == NULL)
	{
		perror("malloc fail");
		return;
	}

	ps1->capacity = 4;
	ps1->size = 0;
}


void SLPrint(SL* ps1)
{
	int i = 0;
	for (i = 0; i < ps1->size; i++) 
	{
		printf("%d ", ps1->a[i]);
	}
	printf("\n");
}

void SLDestroy(SL*ps1)
{
	free(ps1->a);
	ps1->a = NULL;
	ps1->size = 0;
	ps1->capacity = 0;
}

void SLCheckCapacity(SL* ps2)
{
	if (ps2->size == ps2->capacity)
	{
		SLDatatype* tmp = realloc(ps2->a, sizeof(SLDatatype) * ps2->capacity * 2);
		if (tmp == NULL)
		{
			perror("realloc fail");
			return;
		}

		ps2->a = tmp;
		ps2->capacity *= 2;
	}
}

void SLPushBack(SL* ps1, SLDatatype x)
{
	//ps1->a[ps1->size] = x;
	//ps1->size++;

	SLCheckCapacity(ps1);

	ps1->a[ps1->size++] = x;

}
