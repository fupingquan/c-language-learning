#define _CRT_SECURE_NO_WARNINGS 1
//author:付平权
//description:
//time: 

#include <stdio.h>
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	int l = 0;
//	int r = sz - 1;
//
//	while (l<r)
//	{
//		//从前向后找一个偶数
//		while ((l < r) && arr[l] % 2 == 1)
//		{
//			l++;
//		}
//		//从后向前找一个奇数
//		while ((l < r) && arr[r] % 2 == 0)
//		{
//			r--;
//		}
//		if (l < r)
//		{
//			int tmp = arr[l];
//			arr[l] = arr[r];
//			arr[r] = tmp;
//			l++;
//			r--;
//		}
//	}
//
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//
//	return 0;
//}


//int main() {
//    int n = 0;
//    while (scanf("%d", &n) == 1)
//    {
//        int i = 0;
//        int j = 0;
//        for (i = 0; i < n; i++)
//        {
//            for (j = 0; j < n; j++)
//            {
//                if (i == j || i + j == n - 1)
//                    printf("*");
//                else
//                    printf(" ");
//            }
//            printf("\n");
//        }
//    }
//
//    return 0;
//}


////右下三角
//int main()
//{
//    int n = 0;
//    while (scanf("%d", &n) == 1)
//    {
//        int i = 0;
//        int j = 0;
//        for (i = 0; i < n; i++)
//        {
//            for (j = 0; j < n; j++)
//            {
//                if (i + j < n - 1)
//                    printf("  ");
//                else
//                    printf("* ");
//            }
//            printf("\n");
//        }
//    }
//
//    return 0;
//}

////猜名次
//int main()
//{
//    int a = 0;
//    int b = 0;
//    int c = 0;
//    int d = 0;
//    int e = 0;
//    for (a = 1; a <= 5; a++)
//    {
//        for (b = 1; b <= 5; b++)
//        {
//            for (c = 1; c <= 5; c++)
//            {
//                for (d = 1; d <= 5; d++)
//                {
//                    for (e = 1; e <= 5; e++)
//                    {
//                        if (((b == 2) + (a == 3) == 1) &&
//                            ((b == 2) + (e == 4) == 1) &&
//                            ((c == 1) + (d == 2) == 1) &&
//                            ((c == 5) + (d == 3) == 1) &&
//                            ((e == 4) + (a == 1) == 1))
//                        {
//                            if(a*b*c*d*e == 120)
//                                printf("a=%d b=%d c=%d d=%d e=%d\n", a, b, c, d, e);
//                        }
//                    }
//                }
//            }
//        }
//    }
//    return 0;
//}


//int main()
//{
//    char killer = 0;
//    for (killer = 'a'; killer <= 'd'; killer++)
//    {
//        if ((killer!='a') + (killer=='c') + (killer=='d') + (killer!='d') == 3)
//        {
//            printf("凶手是%c\n", killer);
//            break;
//        }
//    }
//}

////似杨辉三角
//int main()
//{
//    int arr[10][10] = { 0 };
//    int i = 0;
//    for (i = 0; i < 10; i++)
//    {
//        int j = 0;
//        for (j = 0; j <= i; j++)
//        {
//            if (i == j || j == 0)
//                arr[i][j] = 1;
//
//            if (i >= 2 && j >= 1)
//            {
//                arr[i][j] = arr[i - 1][j - 1] + arr[i - 1][j];
//            }
//        }
//    }
//    for (i = 0; i < 10; i++)
//    {
//        int j = 0;
//        for (j = 0; j <= i; j++)
//        {
//            printf("%d ", arr[i][j]);
//        }
//        printf("\n");
//    }
//    return 0;
//}


//使用指针数组模拟实现二维数组

//int main()
//{
//	int arr1[] = { 1,2,3,4,5 };//arr1 - int*
//	int arr2[] = { 2,3,4,5,6 };
//	int arr3[] = { 3,4,5,6,7 };
//
//	//指针数组
//	int* arr[3] = { arr1, arr2, arr3 };
//	int i = 0;
//	for (i = 0; i < 3; i++)
//	{
//		int j = 0;
//		for (j = 0; j < 5; j++)
//		{
//			printf("%d ", arr[i][j]);
//		}
//		printf("\n");
//	}
//	return 0;
//}


//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	//数组的地址 - 存储到数组指针变量
//
//	//int[10] * p = &arr; //err
//	int(*p)[10] = &arr;
//
//	return 0;
//}


//int main()
//{
//	int arr[10] = { 0 };
//	printf("%p\n", arr);//
//	printf("%p\n", arr+1);//
//
//	printf("%p\n", &arr[0]);//
//	printf("%p\n", &arr[0]+1);//
//
//	printf("%p\n", &arr);//
//	printf("%p\n", &arr+1);//
//
//	
//	return 0;
//}


//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	int* p = arr; //首元素地址给p
//	int i = 0;
//	for (int i = 0; i < 10; i++)
//	{
//		printf("%d ", *(p + i));
//	}
//	return 0;
//}

int main()
{
	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
	int(*p)[10] = &arr;  //*&arr -->arr;  指针数组
	int i = 0;
	for (i = 0; i < 10; i++)
	{
		//printf("%d ", *((*p) + i));

		printf("%d ", *((*p)+i));
	}
	return 0;
}