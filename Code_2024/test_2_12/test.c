#define _CRT_SECURE_NO_WARNINGS 1
//author:付平权
//description:
//time:

#include <stdio.h>
//int Fun(int n)
//{
//    if (n == 5)
//        return 2;
//    else
//        return 2 * Fun(n + 1);
//}
//
//int main()
//{
//    int ret = Fun(2);
//    printf("%d\n", ret);
//    return 0;
//}

////递归打印数的每一位
//void every_num(int n)
//{
//	if (n > 9)
//	{
//		every_num(n/10);
//	}
//	printf("%d ", n % 10);
//}
//int main()
//{
//	int num = 0;
//	scanf("%d", &num);
//	every_num(num);
//	return 0;
//}


////用递归方式实现打印一个整数的每一位
//#include<stdio.h>
//void print(n)
//{
//	if (n > 9)//限制条件
//		print(n / 10);//(n/10)使其逐渐接近（n>9）这个限制条件
//	printf("%d ", n % 10);
//}
//int main()
//{
//	int input = 0;
//	printf("请输入一个整数：");//1234567890
//	scanf("%d", &input);
//
//	print(input);
//
//	return 0;
//}


////不递归实现n的阶乘
//
////循环(迭代)
//int Fac1(int n)
//{
//	int r = 1;
//	int i = 0;
//	for (i = 1; i <= n; i++)
//	{
//		r = r * i;
//	}
//	return r;
//}
// 
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int ret=Fac1(n);
//	printf("%d", ret);
//	return 0;
//}

//递归
int Fac2(int n)
{
	if (n <= 1)
		return 1;
	else
		return n * Fac2(n - 1);
}

int main()
{
	int n = 0;
	scanf("%d", &n);

	int ret = Fac2(n);
	printf("%d\n", ret);

	return 0;
}