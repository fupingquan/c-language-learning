#define _CRT_SECURE_NO_WARNINGS 1
//author:付平权
//description:
//time:


//在屏幕上打印1-10的数字

#include <stdio.h>

//int main()
//{
//	int i = 0;
//	while (i <=10)
//	{
//		printf("%d ", i);
//		i ++ ;
//	}
//	return 0;
//}

//int main()
//{
//	int i = 0;
//	while (i <= 10)
//	{
//		if (i == 6)
//			break;
//		printf("%d ", i);
//		i++;
//	}
//	return 0;
//}

//int main()
//{
//	int i = 1;
//	while (i <= 10)
//	{
//		if (i == 6)
//			continue;
//		printf("%d ", i);
//		i++;
//	}
//	return 0;
//}


//int main()
//{
//	int i = 1;
//	while (i <= 10)
//	{
//		i = i + 1;
//		if (i == 5)
//			continue;
//		printf("%d ", i);   // 2 3 4 6 7 8 9 10 11 
//	}
//	return 0;
//}


//int main()
//{
//	int i = 1;
//	for (i = 1; i<=10;i++)
//	{
//		printf("%d ", i);
//	}
//	return 0;
//}


//int main()
//{
//	int i = 10;
//	do
//	{
//		printf("%d\n", i);  //10
//	} while (i < 10);
//	return 0;
//}

//int main()
//{
//	int i = 10;
//	do
//	{
//		if (5 == i)
//			break;
//		printf("%d\n", i);
//	} while (i < 10);
//
//	return 0;
//}


//int main()
//{
//    int i = 0;
//    int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//    for (i = 0; i <= 12; i++)  //err
//    {
//        arr[i] = 0;
//        printf("hello bit\n");
//    }
//    return 0;
//}

////打印星号
//int main()
//{
//	int line = 0;
//	scanf("%d", &line);
//	for (int i = 0; i < line;i++)
//	{
//		for (int j = 0; j < line-1-i; j++)
//		{
//			printf(" ");
//		}
//		for (int j = 0; j < 2 * i + 1; j++)
//		{
//			printf("*");
//		}
//		printf("\n");
//	}
//
//	for (int i = 0; i < line - 1; i++)
//	{
//		for (int j = 0; j < i + 1; j++)
//		{
//			printf(" ");
//		}
//		for (int j = 0; j < (line - 1 - i) * 2 - 1; j++)
//		{
//			printf("*");
//		}
//		printf("\n");
//	}
//	return 0;
//}

//拆分一个整数的每一位并打印
void print(int n)
{
	if (n < 10)
		printf("%d ", n);
	else
	{
		print(n / 10);
		printf("%d ", n % 10);
	}
}

int main()
{
	int n = 0;
	scanf("%d", &n);
	print(n);
	return 0;
}


//int i;
//int main()
//{
//	i--;
//	if (i > sizeof(i))
//	{
//		printf(">\n");
//	}
//	else
//	{
//		printf("<\n");
//	}
//	return 0;
//}


int main()
{
	int n = 0;
	int m = 0;
	int arr[10][10] = { 0 };
	scanf("%d %d", &n, &m);
	int i = 0;
	int j = 0;
	for (i = 0; i < n; i++)
	{
		for (j = 0; j < m; j++)
		{
			scanf("%d", &arr[i][j]);
		}
	}
	for (i = 0; i < m; i++)
	{
		for (j = 0; j < n; j++)
		{
			printf("%d ", arr[j][i]);
		}
		printf("\n");
	}

	return 0;
} 