#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
//#include <sys/wait.h>
//#include<sys/types.h>
#include <unistd.h>                                                                                    

int main()
{
    pid_t id=fork();
    if(id==0)
    {
        //子进程
        int cnt=5;
        while(cnt)
        {
            printf("我是子进程：%d\n",cnt--);
            sleep(1);
        }

        exit(11);  //仅用来测试
    }
    else
    {
        //父进程
        int status=0;

        pid_t result=waitpid(id,&status,0);  //默认是在阻塞状态去等待子进程状态变化  （就是退出）
        if(result>0)
        {
            printf("父进程等待成功，退出码：%d,退出信号：%d\n",(status>>8)&0xFF,status&0x7F);
            if(WIFEXITED(status))
            {
                //子进程是正常退出的
                printf("子进程执行完毕，子进程退出码：%d\n",WIFEXITED(status));
            }
            else
            {
                //子进程异常退出
                printf("子进程异常提出：%d\n",WIFEXITED(status));
            }
        }
    }
    return 0;
}

