#define _CRT_SECURE_NO_WARNINGS 1
//author:付平权
//description:
//time:

//模拟实现库函数：strcpy
//拷贝字符串的

#include <stdio.h>
#include <string.h>
//void my_strcpy(char*dest,char*src) //该函数没有考虑到空指针
//{
//	while (*src != '\0')
//	{
//		*dest = *src;
//		dest++;
//		src++;
//	}
//	*dest = *src; //'\0'的拷贝
//}
//
//int main()
//{
//	char arr1[] = "hello pq";
//	char arr2[20] = "xxxxxxxxxx";
//	my_strcpy(arr2, arr1);
//	printf("%s\n", arr2);
//	
//	//printf("%s\n",strcpy(arr2, arr1));
//	return 0;
//}

//#include <assert.h>
//void my_strcpy(char* dest, char* src)
//{
//	//断言，需要包含头文件assert.h
//	assert(dest != NULL);
//	assert(src != NULL);  //遇到空指针可以报错
//
//	while (*src != '\0')
//	{
//		*dest = *src;
//		dest++;
//		src++;
//	}
//	*dest = *src;
//}
//
//int main()
//{
//	char arr1[] = "hello bit";
//	char arr2[] = "xxxxxxxxxxx";
//	my_strcpy(arr2, arr1);
//	printf("%s\n", arr2);
//	return 0;
//}



//函数返回的是目标空间的起始地址
//const的使用

//#include <assert.h>
//void my_strcpy(char* dest, const char* src)
//{
//	//断言
//	assert(dest != NULL);
//	assert(src != NULL);
//
//	//
//	while (*src != '\0')
//	{
//		*dest=*src;
//		dest++;
//		src++;
//	}
//	*dest = *src;
//}
//int main()
//{
//	char arr1[] = "hello bit";
//	char arr2[] = "xxxxxxxxxxx";
//	my_strcpy(arr2, arr1);
//	printf("%s\n", arr2);
//
//	return 0;
//}


//另一种写法
#include <assert.h>
char* my_strcpy(char* dest, const char* src)
{
	char* ret = dest;
	//断言
	assert(dest != NULL);
	assert(src != NULL);
	//assert(*src);

	//
	while (*src != '\0')
	{
		*dest = *src;
		dest++;
		src++;
	}
	*dest = *src;
	return ret;
}
int main()
{
	char arr1[] = "hello bit";
	char arr2[] = "xxxxxxxxxxx";
	/*my_strcpy(arr2, arr1);
	printf("%s\n", arr2);*/

	printf("%s\n", my_strcpy(arr2, arr1));
	return 0;
}
