#define _CRT_SECURE_NO_WARNINGS 1
//author:付平权
//description:
//time: 

#include <stdio.h>

//int main()
//{
//	int line = 0;
//	scanf("%d", &line);
//	//上边
//	for (int i = 0; i < line; i++)
//	{
//		//空格
//		for (int j = 0; j < line - 1 - i; j++)
//		{
//			printf(" ");
//		}
//		//*
//		for (int j = 0; j < 2*i+1; j++)
//		{
//			printf("*");
//		}
//		printf("\n");
//	}
//
//	//下边
//	for (int i = 0; i < line; i++)
//	{
//		//空格
//		for (int j = 0; j <=i; j++)
//		{
//			printf(" ");
//		}
//		//*
//		for (int j = 0; j < 2 * (line-1-i)-1; j++)
//		{
//			printf("*");
//		}
//		printf("\n");
//	}
//	return 0;
//}

////1到100000的自幂数
//#include <math.h>
//int main()
//{
//	for (int i = 0; i < 10000; i++)
//	{
//		//判断i是否是自幂数
//		//1.计算i的位数
//		int n = 1; //记录有几位数
//		int tmp = i;
//		while (tmp /= 10)
//		{
//			n++;
//		}
//		//2.求每一个数的n次方之和
//		double sum=0;
//		tmp = i;
//		while (tmp)
//		{
//			sum += pow(tmp % 10, n);
//			tmp /= 10;
//		}
//		//3.判断
//		if (sum == i)
//		{
//			printf("%d ", i);
//		}
//	}
//	return 0;
//}

////求Sn = a + aa + aaa + aaaa + aaaaa的前5项之和，其中a是一个数字，
////例如：2 + 22 + 222 + 2222 + 22222
//int main()
//{
//	int a = 0;
//	int n = 0;
//	scanf("%d %d", &a, &n);
//	int sum = 0;
//	int k = 0;
//	for (int i = 0; i < n; i++)
//	{
//		k = k * 10 + a;
//		sum += k;
//	}
//	printf("%d", sum);
//	return 0;
//}


//struct stu  //结构体类型
//{
//	int num;  //结构体成员
//	char name[10];
//	int age;
//};
//
//void fun(struct stu * p)
//{
//	printf("%s\n", (*p).name);
//	return;
//}
//
//int main()
//{
//	struct stu student[3] = { {9801,"zhang",20},
//							{9802,"wang",19},
//							{9803,"zhao",18} 
//							};
//	
//	fun(student + 1);
//	return 0;
//}

////喝汽水，1瓶汽水1元，2个空瓶可以换一瓶汽水，给20元，可以多少汽水（编程实现）。
int main()
{
	int money = 0;
	int total = 0;
	int empty = 0;
	scanf("%d", &money);
	total = money;
	empty = money;

	while (empty >= 2)
	{
		total += empty / 2;
		empty = empty / 2 + empty % 2;
	}

	//
	//if (money > 0)
	//	total = 2 * money - 1;

	printf("total=%d\n", total);
	return 0;
}
