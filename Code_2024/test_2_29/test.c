#define _CRT_SECURE_NO_WARNINGS 1
//author:付平权
//description:
//time:

#include <stdio.h>
#include <assert.h>

//struct S
//{
//	char c1;
//	int i;
//	char c2;
//};
//
//#define OFFSETOF(type,member) (size_t)&(((type*)0)->member)
//
//int main()
//{
//	printf("%d\n", OFFSETOF(struct S, c1));
//	printf("%d\n", OFFSETOF(struct S, i));
//	printf("%d\n", OFFSETOF(struct S, c2));
//
//	return 0;
//}


////奇数位为和偶数位交换
//#define SWAP(n) (n=((n&0xaaaaaaaa)>>1)+((n&0x55555555)<<1))
//
//int main()
//{
//	int n = 10;
//	//1010
//	//0101
//	SWAP(n);
//	printf("%d\n", n);
//	return 0;
//
//}



//void Bubblesort(int* a, int n)
//{
//	assert(a);
//	for (size_t end = 0; end > 0; --end)
//	{
//		int exchange = 0;
//		for (size_t i = 1; i < end; ++i)
//		{
//			if (a[i - 1] > a[i])
//			{
//				Swap(&a[i - 1], &a[i]);
//				exchange = 1;
//			}
//		}
//		if (exchange == 0)
//			break;
//	}
//}



//int missingNumber(int* nums, int numsSize)
//{
//	int x = 0;
//	for (int i = 0; i < numsSize; i++)
//	{
//		x ^= nums[i];
//	}
//
//	for (int i = 0; i < numsSize + 1; i++)
//	{
//		x ^= i;
//	}
//	return x;
//}

//找缺失的数字
int missingNumber(int* nums, int numsSize)
{
	int x = (1 + numsSize) * numsSize / 2;

	for (size_t i = 0; i < numsSize; i++)
	{
		x -= nums[i];
	}

	return x;
}