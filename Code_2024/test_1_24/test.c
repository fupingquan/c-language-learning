#define _CRT_SECURE_NO_WARNINGS 1
//author:付平权
//description:
//time:

#include <stdio.h>

//int main()
//{
//	int a = 0; //初始化
//	scanf("%d", &a);
//	if (a == 1)
//	{
//		printf("a等于1");
//	}
//	return 0;
//}

//int main()
//{
//	int b = 0;
//	scanf("%d", &b); //从键盘输入b的值
//	if (b == 1)
//	{
//		printf("b等于1"); //等于1时打印
//	}
//	else
//	{
//		printf("b不等于1"); //不等于1时打印
//	}
//	return 0;
//}


//int main()
//{
//	int a = 0;
//	int b = 0;
//	scanf("%d %d", &a, &b);
//	if (a > b)
//	{
//		printf("%d比%d大\n",a,b);
//	}
//	else if (a < b)
//	{
//		printf("%d比%d大\n",b,a);
//	}
//	else
//	{
//		printf("%d和%d相等\n",a,b);
//	}
//	return 0;
//}


#include <stdio.h>

int main()
{
    char score;
    scanf("%c", &score);
    switch (score) {
    case 'A':
        printf("90分以上\n");
        break;
    case 'B':
        printf("80到90分\n");
        break;
    case 'C':
        printf("70到80分\n");
        break;
    case 'D':
        printf("60到70分\n");
        break;
    case 'E':
        printf("60分以下\n");
        break;
    default:
        printf("输入错误\n");
    }
    return 0;
}

//int* test()
//{
//	int a = 110;
//	return &a;
//}
//
//int main()
//{
//	int* p = test();
//	printf("%d\n", *p);
//
//	return 0;
//}


//int main()
//{
//	int a = 10;
//	int* p = &a;
//
//	int* ptr = NULL;//ptr是一个空指针，没有指向任何有效的空间。这个指针不能直接使用
//	//int* ptr2;//野指针
//
//	if (ptr != NULL)
//	{
//		//使用
//	}
//
//	return 0;
//}
//

//int main()
//{
//	int arr[10] = { 0 };
//	//不使用下标访问数组
//	int* p = &arr[0];
//	int i = 0;
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	for (i = 0; i < sz; i++)
//	{
//		*p = i;
//		p++;//p = p+1
//	}
//	p = arr;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", *(p + i));//p+i
//	}
//
//	/*for (i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}*/
//
//	return 0;
//}