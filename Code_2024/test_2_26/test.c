#define _CRT_SECURE_NO_WARNINGS 1
//author:付平权
//description:
//time: 

#include <stdio.h>
#include <string.h>

//int main()
//{
//	int arr1[] = { 1,2,3,4,5,6,7,8,9,10 };
//
//	memcpy(arr1 + 2, arr1, 20);
//	//            1,2,3,4,5
//	//将arr1中的20个字节替换到arr1+2的位置上
//	//                  1 2 ...        8 9 10
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", arr1[i]);  //1 2 1 2 3 4 5 8 9 10
//	}
//	return 0;
//}


////杨氏矩阵
////1 2 3
////4 5 6
////7 8 9
//void young_tableau_search(int arr[3][3],int k,int*px,int *py)
//{
//	int x = 0;
//	int y = *py - 1;
//	while (x <= *px - 1 && y >= 0)
//	{
//		if (arr[x][y] < k)
//		{
//			x++;
//		}
//		else if (arr[x][y] > k)
//		{
//			y--;
//		}
//		else
//		{
//			*px = x;
//			*py = y;
//			return; //结束
//		}
//	}
//	*px = -1;
//	*py = -1;
//}
//
//int main()
//{
//	int arr[3][3] = { 1,2,3,4,5,6,7,8,9 };
//	//时间复杂度
//	int k = 0;
//	printf("请输入要查找的数:>");
//	scanf("%d", &k);
//
//	int x = 3;
//	int y = 3;
//	young_tableau_search(arr,k,&x,&y);
//
//	if (x == -1 && y == -1)
//		printf("找不到了\n");
//	else
//		printf("找到了下标是%d %d", x, y);
//	return 0;
//}


#include <string.h>
//int main()
//{
//	int arr1[] = { 1,2,3,4,5,6,7,8,9,10 };
//	int arr2[20] = { 0 };
//	//将arr1中的内容拷贝到arr2中
//	memcpy(arr2, arr1, 40); //拷贝40个字节
//	
//	for (int i = 0; i < 20; i++)
//	{
//		printf("%d ", arr2[i]);
//	}
//	return 0;
//}

//int main()
//{
//	float arr1[] = { 1.0,2.0,3.0 };
//	float arr2[5] = { 0 };
//
//	//将arr1中的内容，拷贝到arr2中
//	memcpy(arr2, arr1, 8);
//	//     float* float*
//	int i = 0;
//	for (i = 0; i < 5; i++)
//	{
//		printf("%f ", arr2[i]);
//	}
//
//	return 0;
//}


#include <assert.h>
//函数拷贝结束后，返回目标空间的其实地址
//void* my_memcpy(void*dest,void*src,size_t num)
//{
//	void* ret = dest;  //先把其实地址存起来
//	assert(src && dest);
//	while (num--)
//	{
//		*(char*)dest = *(char*)src;
//		dest = (char*)dest + 1;
//		src = (char*)src + 1;
//	}
//	return ret;
//}
//
//int main()
//{
//	int arr1[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	int arr2[20] = { 0 };
//	my_memcpy(arr2, arr1, 30);
//
//	int i = 0;
//	for (i = 0; i < 20; i++)
//	{
//		printf("%d ", arr2[i]);
//	}
//	return 0;
//}


//void* my_memcpy(void* dest, void* src, size_t num)
//{
//	void* ret = dest;  //先把其实地址存起来
//	assert(src && dest);
//	while (num--)
//	{
//		*(char*)dest = *(char*)src;
//		dest = (char*)dest + 1;
//		src = (char*)src + 1;
//	}
//	return ret;
//}
//
//int main()
//{
//	int arr1[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	int arr2[20] = { 11,11,11,11,11 };
//	my_memcpy(arr1+2, arr2, 16);
//	                  //11 11 11 11
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", arr1[i]); //1, 2, 11, 11, 11, 11, 7, 8, 9, 10;
//	}
//	return 0;
//}


////memmove函数
//int main()
//{
//	int arr1[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	memmove(arr1, arr1 + 2, 20);
//	//                3，4，5，6，7
//	int i = 0;
//	for (i = 0; i < 10; i++) 
//	{
//		printf("%d ", arr1[i]);  //3，4，5，6，5，6，7，8，9，10
//	}
//
//	return 0;
//}



////模拟实现memmove函数
//void* my_memmove(void* dest, const void* src, size_t num)
//{
//	void* ret = dest;
//	assert(dest && src);
//
//	if (dest < src)
//	{
//		while (num--)
//		{
//			//前->后
//			*(char*)dest = *(char*)src;
//			dest = (char*)dest + 1;
//			src = (char*)src + 1;
//		}
//	}
//	else
//	{
//		//后->前
//		while (num--)
//		{
//			*((char*)dest + num) = *((char*)src + num);
//		}
//	}
//	return ret;
//}
//
//int main()
//{
//	int arr1[] = { 1,2,3,4,5,6,7,8,9,10 };
//
//	my_memmove(arr1, arr1 + 2, 20);
//	//               3 4 5 6 7
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", arr1[i]);
//	}
//	return 0;
//}