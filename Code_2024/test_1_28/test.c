#define _CRT_SECURE_NO_WARNINGS 1
//author:付平权
//description:
//time:2024年

//指针的关系运算
#include <stdio.h>
int main()
{
	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
	int* p = &arr[0];
	int i = 0;
	int sz = sizeof(arr) / sizeof(arr[0]);
	while (p < arr + sz) //指针的大小比较
	{
		printf("%d ", *p);
		p++;
	}
	return 0;
}

#include <assert.h>

//void my_strcpy(char* dest, char* src)
//{
//	//断言
//	assert(dest != NULL);
//	assert(src != NULL);
//
//	while (*src != '\0')
//	{
//		*dest = *src;
//		dest++;
//		src++;
//	}
//	*dest = *src;// \0 的拷贝
//}

//函数返回的是目标空间的起始地址
//const 修饰指针的时候
//当const 放在*的左边的时候，限制的是指针指向的内容，不能通过指针变量改变指针指向的内容，但是指针变量的本身是可以改变的
//当const 放在*的右边的时候，限制的是指针变量本身，指针变量的本身是不能改变的，但是指针指向的内容是可以通过指针来改变的
//
//char* my_strcpy(char* dest, const char * src)
//{
//	char* ret = dest;
//	//断言
//	assert(dest != NULL);
//	assert(src != NULL);
//
//	while (*dest++ = *src++) 
//		;//空语句
//
//	return ret;
//}
//
//
//int main()
//{
//	char arr1[] = "hello bit";
//	char arr2[20] = "xxxxxxxxxxxxx";
//	char* p = NULL;
//
//	//my_strcpy(arr2, arr1);
//	//printf("%s\n", arr2);
//
//	printf("%s\n", my_strcpy(arr2, arr1));
//
//	return 0;
//}



//
//int main()
//{
//	//int num = 10;
//	//num = 20;
//
//	//int* p = &num;
//	//*p = 200;
//
//
//	const int n = 100;
//	//n = 200;//err
//
//	const int* p = &n;
//	*p = 20;//
//
//	printf("%d\n", n);
//
//	return 0;
//}
//