#define _CRT_SECURE_NO_WARNINGS 1
//author:付平权
//description:
//time:

#include <stdio.h>

//int Fact(unsigned int n)
//{
//	if (n == 0)
//	{
//		return 1;
//	}
//	else  //n>0
//	{
//		return n* Fact(n - 1);
//	}
//}
 
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int ret=Fact(n); //计算n的阶乘
//	printf("%d \n", ret);
//	return 0;
//}


//void Print(int n)
//{
//	if (n > 9)
//	{
//		Print(n / 10);
//		printf("%d ", n % 10);
//	}
//	else
//	{
//		printf("%d ", n % 10);
//	}
//}

//// 求n的阶乘
//void Print(int n)
//{
//	if (n > 9)
//	{
//		Print(n / 10);
//	}
//	printf("%d ", n % 10);
//}
//
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	Print(n);  //Print函数用来打印n的每一位
//
//	return 0;
//}



//非迭代的方法求n的阶乘
int Fact(int n)
{
	int i = 0;
	int ret = 1;
	for (i = 1; i < n; i++)
	{
		ret *= i;
	}
}

int main()
{
	int n = 0;
	scanf("%d", &n);
	int ret = Fact(n);
	printf("%d\n", ret);

	return 0;
}

