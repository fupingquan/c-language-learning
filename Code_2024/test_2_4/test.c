#define _CRT_SECURE_NO_WARNINGS 1
//author:付平权
//description:
//time:2024-02-04

#include <stdbool.h>
#include <stdio.h>

bool is_Prime(int n)
{
	if (n < 2)
		return false;
	if (n == 2)
		return true;
	for (int i = 3; i * i <= n; i++)
	{
		if (n % 1 == 0)
			return false;
		else
			return true;
	}
}

int main()
{
	int n = 0;
	int ret=is_Prime(n);
	return 0;
}
#if 0
int Is_Leapyear(int n)
{
	if (n % 4 == 0 && n % 100 != 0 || n % 400 == 0)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

int main()
{
	int year;
	printf("请输入年份:");
	scanf("%d", &year);
	int ret=Is_Leapyear(year);
	if (ret == 1)
		printf("%d是闰年\n", year);
	else
		printf("%d不是闰年\n", year);
	return 0;
}


void MulFunc(int n)
{
	for (int i = 1; i <=n; i++)
	{
		for (int j = 1; j <= i; j++)
		{
			printf("%d*%d=%2d ", i, j, i * j);
		}
		printf("\n");
		
	}
}
int main()
{
	int n = 0;
	printf("请输入你要计算的乘法口诀表：");
	scanf("%d", &n);
	MulFunc(n);
	return 0;
}


//二分查找
int bin_search(int arr[], int left, int right, int key)
{
	// arr 是查找的数组
	//left 数组的左下标
	//right 数组的右下标
	//key 要查找的数字
	while (left <= right)
	{
		//int mid = (left + right) / 2;
		int mid = left + (right - left) / 2;
		if (arr[mid] < key)
		{
			left = mid + 1;
		}
		else if (arr[mid] > key)
		{
			right = mid - 1;
		}
		else
		{
			return mid;
			break;
		}
	}
	return -1;		
}

int main()
{
	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
	int k = 8;//在数组中查找8

	int i = 0;
	int sz = sizeof(arr) / sizeof(arr[0]);
	int left = 0;
	int right = sz-1;
	int ret=bin_search(arr, left, right, k);
	if (ret == -1)
	{
		printf("找不到了\n");
	}
	else
		printf("找到了，下标是%d\n", ret);

	return 0;
}



//交换arr1和arr2
int main()
{
	int arr1[9] = { 1,2,3,4,5,6,7,8,9 };
	int arr2[9] = { 10,11,12,13,14,15,16,17,18 };
	int sz = sizeof(arr1) / sizeof(arr1[0]);
	int tmp = 0;
	for (int i = 0; i < sz; i++)
	{
		int tmp = arr1[i];
		arr1[i] = arr2[i];
		arr2[i] = tmp;
	}

	for (int i = 0; i < sz; i++)
	{
		printf("%d ", *(& arr1[i]) );
	}
	printf("\n");
	for (int i = 0; i < 9; i++)
	{
		printf("%d ", *(&arr2[i]));
	}
	return 0;
}

#endif
//20240204