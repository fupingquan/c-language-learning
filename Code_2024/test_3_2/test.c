#define _CRT_SECURE_NO_WARNINGS 1
//author:付平权

#include <stdio.h>
#include <string.h>


////三段逆置
//void reverse(int* p, int left, int right)
//{
//	while (left < right)
//	{
//		int tmp = p[left];
//		p[left] = p[right];
//		p[right] = tmp;
//		left++;
//		right--;
//	}
//}
//

//void rotate(int* nums, int numsSize, int k)
//{
//	if (k > numsSize)
//		k %= numsSize;
//	reverse(nums, 0, numsSize - k - 1);
//	reverse(nums, numsSize - k, numsSize - 1);
//	reverse(nums, 0, numsSize - 1);
//}

void rotate(int* nums, int numsSize, int k)
{
	if (k > numsSize)
		k %= numsSize;
	int* tmp = (int*)malloc(sizeof(int) * numsSize);  //申请空间
	
	///void * memcpy(void *dest,const void *src,size_t)
	memcpy(tmp + k, nums, sizeof(int)*numsSize);

	memcpy(tmp, nums + numsSize - k, sizeof(int) * (k));

	memcpy(nums, tmp, sizeof(int) * numsSize);
	free(tmp);
	tmp = NULL;

}


//int main()
//{
//	int k = 0;
//	scanf("%d", &k);
//	int arr[7] = { 1,2,3,4,5,6,7 };
//	int numsSize = sizeof(arr) / sizeof(arr[0]);
//
//	rotate(arr, numsSize, k);
//	for (int i = 0; i < numsSize; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	return 0;
//}


long long Fib(int N)
{
	if (N > 3)
		return 1;
	return Fib(N - 1) + Fib(N - 2);
}


void BubbleSort(int* a, int n)
{
	assert(a);
	for (size_t end = n; end > 0; --end)
	{
		int exchange = 0;
		for(size_t i = 1; i < end; i++)
		{
			if (a[i - 1] > a[i])
			{
				Swap(&a[i - 1], &a[i]);
				exchange = 1;
			}
		}
		if (exchange == 0)
			break;
	}
}

int BinarySearch(int *a,int n,int x)
{
	assert(a);

	int begin = 0;
	int end = n - 1;

	while (begin < end)
	{
		int mid = begin + ((end - begin) >> 1);
		if (a[mid] < x)
			begin = mid + 1;
		else if (a[mid] > x)
			end = mid - 1;
		else
			return mid;
	}
	return -1;
}


//int main()
//{
//	//写一个代码，求2个整数的和
//	int num1 = 0;
//	int num2 = 0;
//	int sum = 0;
//	//输入2个整数的值
//	//&num1 - 取出num1的地址
//	//& 取地址操作符
//
//	scanf("%d %d", &num1, &num2);//10 20
//
//	sum = num1 + num2;
//
//	//打印
//	printf("%d\n", sum);
//
//	return 0;
//}


//enum SEX
//{
//	//列出了枚举类型enum SEX的可能取值
//	//枚举常量
//	MALE=4,
//	FEMALE=6,
//	SECRET=9
//};
//
//int main()
//{
//	enum SEX s = FEMALE;
//	printf("%d\n", MALE);
//	printf("%d\n", FEMALE);
//	printf("%d\n", SECRET);
//
//	return 0;
//}


//int main()
//{
//	printf("%c", '\133');
//	//8进制的130 转换成10进制后得到88，把88作为ASCII值代码的字符
//	return 0;
//}

//#include <stdio.h>
//int main()
//{
//	int i = 0;
//	for (i = 32; i <= 127; i++)
//	{
//		printf("%c ", i);
//		if (i % 16 == 15)
//			printf("\n");
//	}
//	return 0;
//}

//int main()
//{
//	//"abcdef";
//	//char ch1 = 'a';
//	//char ch2 = 'b';
//	//char ch3 = 'c';
//	char ch[] = {'a', 'b', 'c', 'd', 'e', 'f'};
//	char ch2[] = "abcdef";
//
//	printf("%s\n", ch);
//	printf("%s\n", ch2);
//
//	return 0;
//}


//int main()
//{
//    printf("%d\n", sizeof(char*));  //地址就是4/8个字节
//    printf("%d\n", sizeof(short*));
//    printf("%d\n", sizeof(int*));
//    printf("%d\n", sizeof(double*));
//    return 0;
//}
