#define _CRT_SECURE_NO_WARNINGS 1
//author:付平权
//description:
//time: 

#include <stdio.h>
////二维数组传参，形参是二维数组的形式
//
//void Print(int arr[3][5],int r,int c)
//{
//	int i = 0;
//	for (i = 0; i < 3; i++)
//	{
//		int j = 0;
//		for (j = 0; j < 5; j++)
//		{
//			printf("%d ", arr[i][j]);
//		}
//		printf("\n");
//	}
//}
//
//int main()
//{
//	int arr[3][5] = { 1,2,3,4,5, 2,3,4,5,6, 3,4,5,6,7 };
//	Print(arr, 3, 5);
//	return 0;
//}


////二维数组传参，形参是指针的形式
//void Print(int (*p)[5],int r,int c)
//{
//	int i = 0;
//	for (i=0;i<r;i++)
//	{
//		int j = 0;
//		for (j = 0; j < c; j++)
//		{
//			//*(p+i) 拿到的是一行的数组名
//			//*(p+i)+j 拿到的是这一行下标为j元素的地址
//			printf("%d ", *(*(p + i) + j));
//		}
//		printf("\n");
//	}
//	
//}
//
//int main()
//{
//	int arr[3][5] = { 1,2,3,4,5, 2,3,4,5,6, 3,4,5,6,7 };
//	Print(arr, 3, 5);
//	return 0;
//}

int Add(int x, int y)
{
	return x + y;
}

int Sub(int x, int y)
{
	return x - y;
}

int Mul(int x, int y)
{
	return x * y;
}

int Div(int x, int y)
{
	return x / y;
}

int main()
{
	//int (*pf1)(int ,int) = Add; //pf就是一个函数指针
	//int (*pf2)(int, int) = Sub; //pf就是一个函数指针
	//int (*pf3)(int, int) = Mul; //pf就是一个函数指针
	//int (*pf4)(int, int) = Div; //pf就是一个函数指针


	//创建一个函数指针数组
	int (*ptArr[4])(int, int) = {Add,Sub,Mul,Div};
	//int (*ptArr)(int ,int)是一个函数指针，加上一个方括号就是函数指针数组 int (*ptArr[4])(int, int)
	
	return 0;
}