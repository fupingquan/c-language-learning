#define _CRT_SECURE_NO_WARNINGS 1
//author:付平权
//description:
//time: 

#include <stdio.h>

////非递归
//int my_strlen1(char* s)
//{
//	int count = 0;
//	while (*s != '\0')
//	{
//		count++;
//		s++;
//	}
//	return count;
//}
//
////递归
//int my_strlen(char* s)
//{
//	if (*s == '\0')
//	{
//		return 0;
//	}
//	else
//	{
//		return 1 + my_strlen(s + 1);
//	}
//}
//int main()
//{
//	char arr[] = "abc";
//	int len1 = my_strlen1(arr);
//	printf("字符长度为%d\n", len1);
//
//	int len2 = my_strlen(arr);
//	printf("字符长度为%d\n", len2);
//}

////非递归
//void reverse(int arr[], int sz)
//{
//	int left = 0;
//	int right = sz - 1;
//
//	while (left < right)
//	{
//		int tmp = arr[left];
//		arr[left] = arr[right];
//		arr[right] = tmp;
//		left++;
//		right++;
//	}
//}

//void reverse_string(char* str)
//{
//	int len = strlen(str);
//	char tmp = *str;//将指针指向的字符赋给tmp
//	*str = *(str + len - 1);//将字符串的末尾字符赋给首位字符
//	*(str + len - 1) = '\0';//将末尾字符先暂时置空
//	//下述if语句实现的功能是判断是否到了字符串的中间位置
//	if (strlen(str + 1) >= 2)
//		reverse_string(str + 1);//实现指针所指位置的后移，递归
//	*(str + len - 1) = tmp;//将tmp的值依次赋给被置空的末尾字符
//	/*printf("有递归：%s", str);*/
//}
//
//
//int main()
//{
//	char arr[] = "abcdef";
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	reverse_string(arr,sz);
//
//	return 0;
//}


//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	//int arr[n] = { 0 };
//	int arr[50] = { 0 };
//	for (int i = 0; i < n;i++)
//	{
//		scanf("%d", &arr[i]);
//	}
//	int flag1 = 0; //升序
//	int flag2 = 0; //降序
//	for (int i = 0; i < n - 1; i++)
//	{
//		if (arr[i] < arr[i + 1])
//			flag1 = 1;
//		else if (arr[i] > arr[i + 1])
//			flag2 = 1;
//	}
//
//	if (flag1 + flag2 == 2)
//		printf("unsorted\n");
//	else
//		printf("sorted\n");
//	return 0;
//}

//判断是否有序
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	//int arr[n] = { 0 };
//	int arr[50] = { 0 };
//	int flag1 = 0; //升序
//	int flag2 = 0; //降序
//	for (int i = 0; i < n;i++)
//	{
//		scanf("%d", &arr[i]);
//		if (i >= 1)
//		{
//			if (arr[i] < arr[i - 1])
//				flag1 = 1;
//			else if (arr[i] > arr[i - 1])
//				flag2 = 1;
//		}
//	}
//	if (flag1 + flag2 == 2)
//		printf("unsorted\n");
//	else
//		printf("sorted\n");
//	return 0;
//}

////1  2  3  4  5  6  7  8  9  10 11 12  
////31 28 31 30 31 30 31 31 30 31 30 31
//int get_days_of_month(int y,int m)
//{
//	int d = 0;
//	switch (m)
//	{
//	case 1:
//	case 3:
//	case 5:
//	case 7:
//	case 8:
//	case 10:
//	case 12:
//		d = 31;
//		break;
//	case 4:
//	case 6:
//	case 9:
//	case 11:
//		d = 30;
//		break;
//	case 2:
//	{
//		d = 28;
//		if ((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0))
//			d += 1;
//	}
//	}
//	return d;
//}

int get_days_of_month(int y,int m)
{
	int d = 0;
	int days[] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };
	d = days[m];
	if (((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0)) && m == 2)
		d += 1;
	return d;
}

int main()
{
	int y = 0;
	int m = 0;
	while (scanf("%d %d", &y, &m) == 2)
	{
		int d=get_days_of_month(y, m);
		printf("%d", d);
	}

	return 0;
}

//print(float *p,int sz)
//{
//	float* q = p + sz;
//	while (p < q)
//	{
//		printf("%.2f ", *p++);
//	}
//}

//int main()
//{
//	float arr[] = { 3.14f,99.9f,0.0f };
//	int i = 0;
//	float* p = arr;
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	print(arr,sz);
//	return 0;
//}

 
//int main()
//{
//	char arr[10001] = { 0 };
//	gets(arr);
//	int len = strlen(arr);
//	char* left = arr;
//	char* right = arr + len - 1;
//	while (left < right)
//	{
//		char tmp = *left;
//		*left = *right;
//		*right = tmp;
//		left++;
//		right--;
//	}
//	printf("%s\n", arr);
//	return 0;
//}