#define _CRT_SECURE_NO_WARNINGS 1
//author:付平权
//description:
//time:

//const--常变量，常值属性
#include <stdio.h>
//int  main()
//{
//	int n = 10;
//	int* p = &n;
//	*p = 100;
//	printf("%d\n", n);
//	return 0;
//}

//int main()
//{
//	int n = 5;
//	int m = 2;
//	const int* p = &n;
//	//*p = 100; //err
//	p = &m;
//	return 0;
//}


//int main()
//{
//	int n = 3;
//	int m = 10;
//	const int* const p = &n;
//	//*p = 100;  //err
//	//p = &m;    //err
//	return 0;
//}


//int main()
//{
//	int a = 100;
//	int * pa = &a;//pa是专门用来存放地址（指针）的，这里的pa就被称为指针变量
//	char* pc = &a;
//
//	//指针变量在32为平台下是4个字节
//	//指针变量在64为平台下是8个字节
//
//	//int arr[10];
//	//printf("%p\n", &a);
//
//	return 0;
//}

//
//int main()
//{
//	printf("%d\n", sizeof(char*));
//	printf("%d\n", sizeof(short*));
//	printf("%d\n", sizeof(int*));
//	printf("%d\n", sizeof(long*));
//	printf("%d\n", sizeof(float *));
//	printf("%d\n", sizeof(double*));
//
//	return 0;
//}
//

//指针类型的意义

//int main()
//{
//	int a = 0x11223344;//0x开头的是16进制数字
//	char* pa = &a;
//	*pa = 0;
//	return 0;
//}

//指针类型决定了指针解引用操作的权限


//int main()
//{
//	int a = 0x11223344;//0x开头的是16进制数字
//
//	int* pa = &a;
//	char* pc = &a;
//	printf("%p\n", pa);
//	printf("%p\n", pc);
//
//	printf("%p\n", pa+1);
//	printf("%p\n", pc+1);
//
//	return 0;
//}
