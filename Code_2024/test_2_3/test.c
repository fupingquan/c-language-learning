#define _CRT_SECURE_NO_WARNINGS 1
//author:付平权
//description:
//time:

#include <stdio.h>

#if 0 //条件编译

#include <stdlib.h>
#include <time.h>
#include <stdio.h>

void menu()
{
	printf("********************************\n");
	printf("*******     1. play      *******\n");
	printf("*******     0. exit      *******\n");
	printf("********************************\n");
}

void game()
{
	//RAND_MAX-32767
	//1.生成随机数
	//讲解rand函数
	int ret = rand() % 100 + 1;
	int num = 0;
	//2.猜数字
	while (1)
	{
		printf("请猜数字:>");
		scanf("%d", &num);
		if (num == ret)
		{
			printf("恭喜你，猜对了\n");
			break;
		}
		else if (num > ret)
		{
			printf("猜大了\n");
		}
		else
		{
			printf("猜小了\n");
		}
	}
}

int main()
{
	int input = 0;
	//讲解srand函数
	srand((unsigned int)time(NULL));
	do
	{
		menu();
		printf("请选择:>");
		scanf("%d", &input);
		switch (input)
		{
		case 1:
			game();
			break;
		case 0:
			printf("退出游戏\n");
			break;
		default:
			printf("选择错误\n");
			break;
		}
	} while (input);
	return 0;
}


//int Day(int year, int month)
//{
//	int arr[] = { 31,28,31,30,31,30,31,31,30,31,30,31 };
//
//	int day = arr[month - 1];
//
//	//判断闰年
//	if (year % 4 == 0 && year % 100 != 0 || year % 400 == 0)
//	{
//		if (month == 2)
//		{
//			day += 1;
//		}
//	}
//	return day;
//}
//
//int main()
//{
//	int year, month;
//	while (scanf("%d %d", &year, &month)!=EOF)
//	{
//		int ret = Day(year, month);
//		printf("%d\n", ret);
//	}
//	return 0;
//}


//int Add(int x, int y)
//{
//	return x + y;
//}
//
//int Sub(int x, int y)
//{
//	return x - y;
//}
//
//int Mul(int x, int y)
//{
//	return x * y;
//}
//
//int Div(int x, int y)
//{
//	return x / y;
//}
//
//void menu()
//{
//	printf("***************************\n");
//	printf("****  1. add  2. sub   ****\n");
//	printf("****  3. mul  4. div   ****\n");
//	printf("****  0. exit          ****\n");
//	printf("***************************\n");
//}
//int main()
//{
//	int input = 0;
//	int x = 0;
//	int y = 0;
//	int z = 0;
//	//函数指针的数组 - 转移表
//	int (*pfArr[5])(int, int) = { 0,   Add, Sub, Mul, Div };
//	//                            0    1    2    3    4   
//	do
//	{
//		menu();
//		printf("请选择：");
//		scanf("%d", &input);//3
//
//		if (input >= 1 && input <= 4)
//		{
//			printf("请输入两个操作数:");
//			scanf("%d %d", &x, &y);
//			z = pfArr[input](x, y);
//			printf("%d\n", z);
//		}
//		else if (input == 0)
//		{
//			printf("退出计算器\n");
//		}
//		else
//		{
//			printf("输入错误，重新输入\n");
//		}
//	} while (input);
//
//	return 0;
//}

//void FindNum(int arr[],int n,int *pnum1,int *pnum2)
//{
//	//1. 将整个数组异或起来，得到两个不同数字的异或结果  例如：5^6的结果
//	int tmp = 0;
//	for (int i = 0; i < n; i++)
//	{
//		tmp ^= arr[i];
//	}
//	//2.找到tmp中，二进制为1的某一位k    0110  0111  0001 
//	int k = 0;
//	for (int i = 0; i < n; i++)
//	{
//		if ((tmp >> i & 1) != 0)
//		{
//			k = i;
//			break;
//		}
//	}
//	//3、遍历数组 把每个数据 第K位上是1的，分到一个组进行异或
//	//最终的值存储到 pnum1 或者  pnum2 当中 
//	for (int i = 0; i < n; i++)
//	{
//		if ((arr[i] >> k & 1) != 0)
//		{
//			//第K位是1
//			*pnum1 ^= arr[i];
//		}
//		else
//		{
//			//第K位是0
//			*pnum2 ^= arr[i];
//		}
//	}
//}
//
//int main()
//{
//	int arr[10] = { 1,2,3,4,5,1,2,3,4,6 };
//	int len = sizeof(arr) / sizeof(arr[0]);
//	int num1 = 0;
//	int num2 = 0;
//	FindNum(arr, len, &num1, &num2);
//	printf("%d %d\n", num1, num2);
//
//	return 0;
//}

/*
思路：
1. 采用循环的方式输入一个数组
2. 使用max标记数组中的最大值，采用循环的方式依次获取数组中的每个元素，与max进行比较，如果arr[i]大于    max，更新max标记的最大值，数组遍历结束后，max中保存的即为数组中的最大值。
*/
int main()
{
	int arr[10] = { 0 };
	int i = 0;
	int max = 0;

	for (i = 0; i < 10; i++)
	{
		scanf("%d", &arr[i]);
	}
	//
	max = arr[0];
	for (i = 1; i < 10; i++)
	{
		if (arr[i] > max)
			max = arr[i];
	}
	printf("max = %d\n", max);
	return 0;
}

int main()
{
	for (int i = 1; i <= 9; i++)
	{
		for (int j = 1; j <= i; j++)
		{
			printf("%d*%d=%2d ", j, i, i * j);
		}
		printf("\n");
	}
	return 0;
}

#endif