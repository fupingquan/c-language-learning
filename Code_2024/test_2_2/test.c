#define _CRT_SECURE_NO_WARNINGS 1
//author:付平权
//description:
//time:

#include <stdio.h>

//int main()
//{
//	int a = 0;
//	int b = 0;
//	scanf("%d %d", &a, &b);
//	int m = a / b;
//	int n = a % b;
//	printf("%d %d\n", m, n);
//
//	return 0;
//}

//int main()
//{
//	int n = 0;
//	while (scanf("%d", & n) != EOF)
//	{
//		if (n > 140)
//			printf("Genius");
//	}
//	return 0;
//}

//int main()
//{
//	int num1 = 0;
//	int num2 = 0;
//	while (scanf("%d%d", &num1, &num2) != EOF)
//	{
//		if (num1 > num2)
//			printf("%d>%d", num1, num2);
//		else if (num1 < num2)
//			printf("%d<%d", num1, num2);
//		else
//			printf("%d=%d", num1, num2);
//	}
//	return 0;
//}


//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	if (n % 5 == 0)
//		printf("YES\n");
//	else
//		printf("NO\n");
//	return 0;
//}


//int main()
//{
//	int n = 0;
//	while (scanf("%d", &n) != EOF)
//	{
//		int i = 0;
//		for (i = 0; i < n; i++)
//		{
//			printf("*");
//		}
//		printf("\n");
//	}
//	return 0;
//}

//int main()
//{
//	int i = 0;
//	for (i = 1; i <= 100; i++)
//	{
//		if (i % 3 == 0)
//		{
//			printf("%d ", i);
//		}
//	}
//	return 0;
//}

//int main()
//{
//	int a = 2;
//	int b = 3;
//	int c = 1;
//	scanf("%d%d%d", &a, &b, &c);
//	if (a < b)
//	{
//		int tmp = a;
//		a = b;
//		b = tmp;
//	}
//	if (a < c)
//	{
//		int tmp = a;
//		a = c;
//		c = tmp;
//	}
//	if (b < c)
//	{
//		int tmp = b;
//		b = c;
//		c = tmp;
//	}
//	printf("a=%d b=%d c=%d", a, b, c);
//	return 0;
//
//}


//int main()
//{
//	int a = 0;
//	int b = 0;
//	int c = 0;
//	while (scanf("%d %d %d", &a, &b, &c) != EOF)
//	{
//		if ((a + b > c) && (a + c > b) && (b + c > a))
//		{
//			//三角形
//			if (a == b && b == c) //等边三角形
//			{
//				printf("Equilateral triangle!\n");
//			}
//			else if (((a == b) && (b != c)) || ((a == c) && (c != b)) || ((b == c) && (c != a))) //等腰
//			{
//				printf("Isosceles triangle!\n");
//			}
//			else
//			{
//				printf("Ordinary triangle!\n");
//			}
//		}
//		else
//		{
//			//不是三角形
//			printf("Not a triangle!\n");
//		}
//	}
//	return 0;
//}


//int main()
//{
//	int i = 0;
//	double sum = 0;
//	int flag = 1;
//	for (i = 1; i < +100; i++)
//	{
//		sum+=flag*1.0 / i;
//		flag = -flag;
//	}
//	printf("%lf", sum);
//}


//int main()
//{
//	int i = 0;
//	int count = 0;
//	for (i = 1; i <= 100; i++)
//	{
//		if (i % 10 == 9)
//			count++;
//		if (i / 10 == 9)
//			count++;
//	}
//	printf("%d\n", count);
//	return 0;
//}

//const 修饰指针的时候
//当const 放在*的左边的时候，限制的是指针指向的内容，不能通过指针变量改变指针指向的内容，但是指针变量的本身是可以改变的
//当const 放在*的右边的时候，限制的是指针变量本身，指针变量的本身是不能改变的，但是指针指向的内容是可以通过指针来改变的

//int main()
//{
//	int m = 10;
//	//cosnt 可以修饰指针
//	int n  = 100;
//	const int * p = &m;
//	//*p = 0;//err
//	p = &n; //ok
//
//	printf("%d\n", m);
//
//	return 0;
//}

//
//int main()
//{
//	int m = 10;
//	//cosnt 可以修饰指针
//	int n = 100;
//	int * const p = &m;
//	*p = 0;//ok
//	p = &n; //err
//
//	printf("%d\n", m);
//
//	return 0;
//}
//

//模拟实现一个strlen函数
//assert
//const

//size_t 是专门为sizeof 设计的一个类型
//size_t  unsigned int / unsigned long 
//>=0
//size_t my_strlen(const char* str)
//{
//	assert(str);
//	size_t count = 0;
//	while (*str)
//	{
//		count++;
//		str++;
//	}
//	return count;
//}
//
//int main()
//{
//	char arr[] = "abc";
//	size_t len = my_strlen(arr);
//	printf("%zd\n", len);
//
//	return 0;
//}
//
//%u 无符号整数的
//
//

//int Add(int x, int y)
//{
//	return x - y;
//}
//int main()
//{
//	int ret = Add(2, 3);
//	printf("%d\n", ret);
//	return 0;
//}
//